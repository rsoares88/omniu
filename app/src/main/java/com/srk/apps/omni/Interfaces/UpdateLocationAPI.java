package com.srk.apps.omni.Interfaces;

import com.srk.apps.omni.SyncService.HasService;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * OMNI
 * by SRK on 27-01-2016 @ 02:42
 *
 * @SYNAPPS 2016
 */
public interface UpdateLocationAPI {
    @GET("LocationService.php?action=action&lat=lat&lon=lon&userID=userID")
    Call<HasService> getResult(
            @Query("action") String action,
            @Query("lat") String lat,
            @Query("lon") String lon,
            @Query("userID") Integer userID);
}
