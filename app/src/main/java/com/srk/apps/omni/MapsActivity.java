package com.srk.apps.omni;

import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    public static MapsActivity instance;

    private GoogleMap mMap;
    private Location current_location;

    private Circle mCircle;
    private Marker mMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        instance = this;
        Log.e("MAPS", "DEBUG MAP IS OPENING");
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        current_location = BackgroundService.instance.getLocation();
        // Add a marker in Sydney and move the camera

        if (current_location != null) {
            LatLng position = new LatLng(current_location.getLatitude(), current_location.getLongitude());
            old_marker = mMap.addMarker(new MarkerOptions().position(position).title("Current Location"));
            mMarker = old_marker;
            mMap.moveCamera(CameraUpdateFactory.newLatLng(position));


            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(position)      // Sets the center of the map to Mountain View
                    .zoom(20)                   // Sets the zoom
                    .build();                   // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        }
        handler.postDelayed(runnable, 1000);
    }
    Handler handler = new Handler();

    Runnable runnable = new Runnable() {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            //Log.d("MAPS", "UPDATE MAPS");
            UpdateMarker();
            //handler.postDelayed(this, 1000); // 1000 - Milliseconds
        }
    };

    Boolean FIRST_RUN = true;
    Marker old_marker;
    TextView TextTimer;
    int TimePassed = 0;
    void UpdateMarker(){
        TimePassed++;
        TextTimer = (TextView) findViewById(R.id.updateTimer);
        TextTimer.setText(String.valueOf(TimePassed) + " (" + String.valueOf(TimePassed/60) + ") ");

        //Log.d("MAPS", "UPDATING MAP LOCATION");
        //get new location
        Location new_location = BackgroundService.instance.getLocation();
        if(new_location == null) {
            //Log.d("MAPS", "current_location is NULL");
            handler.postDelayed(runnable, 1000);
            return;
        }
         //if new location != currentLocation
        if(new_location != current_location || FIRST_RUN) {
            FIRST_RUN = false;
            TextTimer.setText(TextTimer.getText() + " AC: " + new_location.getAccuracy());
           // Log.d("MAPS", "GOT NEW LOCATION");
            current_location = new_location;
            //create new marker at new location pos

            if(old_marker !=null) {
                //Log.d("MAPS", "EDITING OLD MARKER");
                old_marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            }

            LatLng position = new LatLng(current_location.getLatitude(), current_location.getLongitude());
            Marker new_marker = mMap.addMarker(new MarkerOptions()
                    .position(position)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));


            //create new marker at new position
            old_marker = new_marker;

            //Log.d("MAPS", "MOVE CAMERA");
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(position)      // Sets the center of the map to Mountain View
                    .zoom(20)                   // Sets the zoom
                    .build();                   // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            if(mCircle == null || mMarker == null){
                drawMarkerWithCircle(position);
            }else{
                updateMarkerWithCircle(position);
            }

        } else {
            //Log.d("MAPS", "GOT OLD LOCATION");
        }
        handler.postDelayed(runnable, 1000);
    }

    private Circle nCircle;
    private void updateMarkerWithCircle(LatLng position) {
        nCircle.setCenter(position);
        //mMarker.setPosition(position);

    }

    private void drawMarkerWithCircle(LatLng position){
        // latlon 38.762093 -  -9.115188 // default position
        /*
        LatLng default_position = new LatLng(38.762093, -9.115188);
        double radiusInMeters = 20.0;
        int strokeColor = 0xffff0000; //red outline
        int shadeColor = 0x44ff0000; //opaque red fill

        CircleOptions circleOptions = new CircleOptions().center(default_position).radius(radiusInMeters).fillColor(shadeColor).strokeColor(strokeColor).strokeWidth(8);
        mCircle = mMap.addCircle(circleOptions);
        */
        Location accuracy = BackgroundService.instance.getLocation();
        double nradiusInMeters = accuracy.getAccuracy();
        int nstrokeColor = 0xffffffff; //white outline
        int nshadeColor = 0x44ff0000; //opaque red fill
        int mstrokeColor = 0x20ffffff; //opaque white fill
        if(mCircle != null) {
            mCircle.setFillColor(mstrokeColor);
        }
        CircleOptions ncircleOptions = new CircleOptions().center(position).radius(nradiusInMeters).fillColor(nshadeColor).strokeColor(nstrokeColor).strokeWidth(3);
        nCircle = mMap.addCircle(ncircleOptions);
        mCircle = nCircle;

        //MarkerOptions markerOptions = new MarkerOptions().position(position);
        //mMarker = mMap.addMarker(markerOptions);

    }

}
