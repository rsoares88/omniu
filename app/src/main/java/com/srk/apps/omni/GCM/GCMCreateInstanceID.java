package com.srk.apps.omni.GCM;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.srk.apps.omni.R;
import com.srk.apps.omni.StorageClass.StorePrefs;

import java.io.IOException;

/**
 * OMNI
 * by SRK on 03-02-2016 @ 20:54
 *
 * @SYNAPPS 2016
 */
public class GCMCreateInstanceID extends AsyncTask<Void, Void, String> {

    private static final String TAG = "GCMRelated";
    Context ctx;
    GoogleCloudMessaging gcm;
    String GCM_SENDER_ID; // (ok)
    String gcmRegId = null;
    String appVersion = null;
    //private int appVersion;

    public GCMCreateInstanceID(Context ctx, GoogleCloudMessaging gcm){
        this.ctx = ctx;
        this.gcm = gcm;
        //this.appVersion = appVersion;
        this.GCM_SENDER_ID = ctx.getResources().getString(R.string.GCM_SENDER_ID);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected String doInBackground(Void... params) {
        // TODO Auto-generated method stub
        if (gcm == null) {
            gcm = GoogleCloudMessaging.getInstance(ctx);
        }
        try {
            //gcmRegId = gcm.register(GCM_SENDER_ID);
            String iid = InstanceID.getInstance(ctx).getId();
            InstanceID thisInstance = InstanceID.getInstance(ctx);
            thisInstance.deleteInstanceID();
            thisInstance.deleteToken(this.GCM_SENDER_ID, "GCM");

            gcmRegId = thisInstance.getToken(this.GCM_SENDER_ID, "GCM");
            //gcmRegId = gcm.register(this.GCM_SENDER_ID);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Log.d(TAG, "GOT INSTANCEID: " + gcmRegId);
        return gcmRegId;
    }

    @Override
    protected void onPostExecute(String result) {
        if (result != null) {
            StorePrefs storage = new StorePrefs();
            int version = storage.getAppVersion(this.ctx);
           storage.SaveGCMInstanceID(result, version, this.ctx);

        } else {
            Log.e(TAG, "FAILED TO GET GCM-ID");
        }
    }


}
