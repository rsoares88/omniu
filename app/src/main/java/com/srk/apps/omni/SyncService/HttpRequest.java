package com.srk.apps.omni.SyncService;

import android.os.AsyncTask;
import android.util.Log;

import com.srk.apps.omni.Activitys.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * OMNI
 *  by SRK on 17-01-2016 @ 02:48
 * @SYNAPPS 2016
 */
/*
public class HttpRequest extends Activity {

    private String TAG = "HTTP-REQUEST";
    private String Serverlink = "http://activetest.pcriot.com/omniu/"; //LoginClass.php";

    private Context context;
    private int byGetOrPost = 0;
*/
    //  new HttpRequest(this, "null", null, 0).execute("router.php", "has_service")

    // When user clicks button, calls AsyncTask.
    // Before attempting to fetch the URL, makes sure that there is a network connection.
    /*
    public Boolean HttpRequest(Context ctx, int GetOrPost) {
        // Gets the URL from the UI's text field.
        String stringUrl = Serverlink;
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            new DownloadWebpageTask().execute(stringUrl);
            return true;
        } else {
            return false;
        }
    }
*/
    // Uses AsyncTask to create a task away from the main UI thread. This task takes a
    // URL string and uses it to create an HttpUrlConnection. Once the connection
    // has been established, the AsyncTask downloads the contents of the webpage as
    // an InputStream. Finally, the InputStream is converted into a string, which is
    // displayed in the UI by the AsyncTask's onPostExecute method.
    public class HttpRequest extends AsyncTask<String, Void, String> {
    private String TAG = "HTTP-REQUEST";
    private String Serverlink = "http://activetest.pcriot.com/omniu/"; //LoginClass.php"; "http://activetest.pcriot.com/omniu/"

    @Override
    protected String doInBackground(String... args) {

        // params comes from the execute() call: params[0] is the url.
/*
            if(args[0] != null) //check if there is at least one destination page
            {
                switch (byGetOrPost){
                    case 0: {
                        //EXECUTE GET REQUEST
                    }
                    case 1: {
                        //EXECUTE POST REQUEST
                    }
                }
                return "true";
            }
            else return "false";
//

            String POST_PARAMS = "action=" + args[0] + "&param2=" + args[1];
            URL obj = null;
            HttpURLConnection con = null;
            try {
                obj = new URL(Serverlink+"?action=has_service");
                con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");

                // For POST only - BEGIN
                con.setDoOutput(true);
                OutputStream os = con.getOutputStream();
                os.write(POST_PARAMS.getBytes());
                os.flush();
                os.close();
                // For POST only - END

                int responseCode = con.getResponseCode();
                Log.i(TAG, "POST Response Code :: " + responseCode);

                if (responseCode == HttpURLConnection.HTTP_OK) { //success
                    BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    String inputLine;
                    StringBuffer response = new StringBuffer();

                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    in.close();

                    // print result
                    Log.i(TAG, response.toString());
                } else {
                    Log.i(TAG, "POST request did not work.");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
*/

        InputStream is = null;
        URL url;
        // Only display the first 500 characters of the retrieved
        // web page content.
        int len = 1024;  //TODO TRY getBytes("UTF-8") TO GET SIZE

        try {
            //http://activetest.pcriot.com/omniu/router.php?&action=has_service
            url = new URL(Serverlink + "router.php?action=has_service");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is: " + response);
            is = conn.getInputStream();

            // Convert the InputStream into a string

            Log.e(TAG, "STREAM READ: " + is.toString());
            Log.e(TAG, "RESULT LENGTH: " + is.toString().length() + "/" + len);
            String contentAsString = readIt(is, len);
            Log.e(TAG, "CONTENT message: " + conn.getResponseMessage() + "/" + len);
            return contentAsString;

            // Makes sure that the InputStream is closed after the app is
            // finished using it.
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return "failed";
    }

    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(String rawResult) {
        Log.i(TAG, "POST request did work: " + rawResult);
        JSONObject resultObject = null;
        try {

            resultObject = new JSONObject(rawResult);
            String action = (String) resultObject.get("action");

            switch (action) {
                case "has_service": {
                    Log.i(TAG, "POST ACTION has_service: " + resultObject.get("result"));
                    String result = (String) resultObject.get("result");
                    HasService(result);
                    break;
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(resultObject == null){
            MainActivity.mainactivity.no_service("Timed Out", "The servers are offline, retry again?");
        }
        Log.i(TAG, "GET RESULT OBJ " + resultObject);
    }


    private void HasService(String value) {
        if (value.equals("online")) {
            //MAIN SERVERS ARE ONLINE PROCEED
            Log.i(TAG, "has_service: " +value);
            MainActivity.mainactivity.has_Service();
        }
        if (value.equals("maintenance")) {
            //MAIN SERVER IS IN MAINTENANCE
            MainActivity.mainactivity.no_service("Maintenance", "The servers are offline, we promise to be short!");
            //no_service
        }
        if (!value.equals("maintenance") && !value.equals("online")) {
            MainActivity.mainactivity.no_service("ERROR: 10", "Invalid response, try again later.");
        }
    }

    // Reads an InputStream and converts it to a String.
    public String readIt(InputStream stream, int len) throws IOException {
        Reader reader;
        reader = new InputStreamReader(stream, "UTF-8");
        char[] buffer = new char[len];
        reader.read(buffer);
        return new String(buffer);
    }

}
//}
