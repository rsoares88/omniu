package com.srk.apps.omni.Activitys;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.squareup.picasso.Picasso;
import com.srk.apps.omni.Helpers.AvatarsList;
import com.srk.apps.omni.Helpers.CategoriesGrid;
import com.srk.apps.omni.Helpers.CircleImageView;
import com.srk.apps.omni.Interfaces.RegisterUserAPI;
import com.srk.apps.omni.Krypton.Kryptonite;
import com.srk.apps.omni.R;
import com.srk.apps.omni.StorageClass.StorePrefs;
import com.srk.apps.omni.StorageClass.User;
import com.srk.apps.omni.SyncService.RegisterUser;
import com.srk.apps.omni.SyncService.UploadImage;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;

public class Create_Profile extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    //TODO DATE FIELD, OPEN DATE PICKER ON FOCUS
    public static Create_Profile create_profile;
    public static User User_Data;
    public static List<Long> SelectedValues = new ArrayList<>();
    private static int RESULT_LOAD_IMAGE = 1;
    public String API_BASE_URL;
    public boolean EXIT_REQUEST = false;
    Boolean is_fb_login = false;
    String cached_avatar;
    String cached_cover;
    boolean IsRegistering = false;
    //TODO FAILED UPLOADS
    Boolean failed_uploads = false;

    Boolean uploaded_avatar = false;
    Boolean uploaded_cover = false;
    String RegisterMsg = "Account Created";
    Integer REG_REPLY_CODE = 500;
    private Integer CURRENT_STEP = 0;
    private DatePickerDialog fromDatePickerDialog;
    private SimpleDateFormat dateFormatter;
    private int mYear;
    private int mMonth;
    private int mDay;
    private FrameLayout avatarUpload;
    private CircleImageView profile_upload;
    private FrameLayout stepsBar;
    private ImageView currStep;
    private ImageView cover;
    private TextView coverUpl_text;
    private CircleImageView avatar;
    private LinearLayout step_0;
    private EditText userName;
    private EditText userEmail;
    private EditText passWord;
    private LinearLayout step_1;
    private EditText fb_userName;
    private LinearLayout step_2;
    private EditText firstName;
    private EditText lastName;
    private EditText birthDate;
    private LinearLayout step_3;
    private EditText userAboutU;
    private RelativeLayout step_4;
    private GridView catGrid;
    private FrameLayout errorFrame;
    private CircleImageView profile_image;
    private TextView topic_txt;
    private CircleImageView private_avatar;

    //TODO [MUST READ] ADD STEP 1 - ?????
    private TextView result_txt;
    private Button btn_retry;
    private String UploadAction;
    private Integer faild_count = 0;
    private int retry_code = 0;

    private Handler mHandler = new Handler();
    private final Runnable VerifyUploads = new Runnable() {
        @Override
        public void run() {
            if (!uploaded_avatar && !uploaded_cover) {
                DisplayErrorFrame("Please Wait", "Uploading Images");
                mHandler.postDelayed(VerifyUploads, 1000);
            } else {
                //finished registering
                DisplayErrorFrame("REGISTRATION SUCCESS", RegisterMsg.toString());
                ToggleErrorFrameButtons(1);
                // set profile_image
                Picasso.with(create_profile)
                        .load(User_Data.Avatar_Img_Link)
                        .placeholder(R.drawable.ic_placeholder) // optional
                        .error(R.drawable.ic_error_fallback)         // optional
                        .into(profile_image);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //TODO TEST CODE
        API_BASE_URL = getApplicationContext().getString(R.string.API_BASE_URL);
        //END TEST CODE
        setContentView(R.layout.activity_create__profile);
        create_profile = this;
        android.support.v7.app.ActionBar AB = getSupportActionBar();
        if (AB != null) {
            AB.hide();
        }

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        User userdata = (User) extras.getSerializable("userdata");
        Boolean isFacebook = extras.getBoolean("isFacebook");

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

        findViewsById();

        if (isFacebook) {
            //initialize as facebook view (fields are initially set with fb data)
            User_Data = userdata;
            InitializeAsFacebook();
        } else {
            //initialize as email view
            User_Data = new User();
            InitializeAsEmail();
        }
    }

    //TODO CHANGE TO FALSE FOR PRODUCTION (false)
    protected Boolean isDebug = false;

    private void debug_helper() {
        if (isDebug) {
            userName.setText("serialkiler");
            userEmail.setText("ricardo.mxp@gmail.com");
            passWord.setText("20039292Az");
            firstName.setText("Ricardo");
            lastName.setText("Soares");
            birthDate.setText("03-06-1988");
            userAboutU.setText("Sample text, delete debug helper function");
        }
    }

    private void findViewsById() {
        avatarUpload = (FrameLayout) findViewById(R.id.avatarUpload);
        currStep = (ImageView) findViewById(R.id.currStep);
        coverUpl_text = (TextView) findViewById(R.id.coverUpl_text);
        profile_upload = (CircleImageView) findViewById(R.id.profile_upload);

        step_0 = (LinearLayout) findViewById(R.id.step_0);
        userName = (EditText) findViewById(R.id.userName);
        userEmail = (EditText) findViewById(R.id.userEmail);
        passWord = (EditText) findViewById(R.id.passWord);

        step_1 = (LinearLayout) findViewById(R.id.step_1);
        fb_userName = (EditText) findViewById(R.id.fb_userName);

        step_2 = (LinearLayout) findViewById(R.id.step_2);
        stepsBar = (FrameLayout) findViewById(R.id.stepsBar);
        cover = (ImageView) findViewById(R.id.cover_img);
        avatar = (CircleImageView) findViewById(R.id.profile_image);
        firstName = (EditText) findViewById(R.id.firstName);
        lastName = (EditText) findViewById(R.id.lastName);
        birthDate = (EditText) findViewById(R.id.birthDate);
        birthDate.setInputType(InputType.TYPE_NULL); //TODO THIS IS A TEST CODE, REMOVE IF DOESNT WORK

        step_3 = (LinearLayout) findViewById(R.id.step_3);
        userAboutU = (EditText) findViewById(R.id.userAboutU);

        step_4 = (RelativeLayout) findViewById(R.id.step_4);

        errorFrame = (FrameLayout) findViewById(R.id.errorFrame);
        profile_image = (CircleImageView) findViewById(R.id.profile_image);
        private_avatar = (CircleImageView) findViewById(R.id.private_avatar);
        topic_txt = (TextView) findViewById(R.id.topic_txt);
        result_txt = (TextView) findViewById(R.id.result_txt);
        btn_retry = (Button) findViewById(R.id.btn_retry);


        step_0.requestFocus();
        //TODO REMOVE FOR PRODUCTION (INITIALIZES THE FIELDS WITH MY USER DETAILS)
        debug_helper();
    }

    public void setDateTimeField(View view) {
        Log.d("DATE", "CLICKED DATE TEXT EVENT");
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpd = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        String dateText = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                        birthDate.setText(dateText);
                    }
                }, mYear, mMonth, mDay);
        dpd.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        String dateText = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
        birthDate.setText(dateText);
    }


    private void InitializeAsFacebook() {
        //POPULATE FACEBOOK DATA, JUMP TO STEP_1 (ASK FOR USER NAME);
        firstName.setText(User_Data.FirstName);
        lastName.setText(User_Data.LastName);
        birthDate.setText(User_Data.BirthDate);

        //new ImageLoadTask(User_Data.Avatar_Img_Link, avatar).execute();
        //new ImageLoadTask(User_Data.Cover_Img_Link, cover).execute();

        //NextStep(1);
        CURRENT_STEP = 1;
        ChangeStepBar();
        stepsBar.setVisibility(View.VISIBLE);
        step_1.setVisibility(View.VISIBLE);
    }

    private void InitializeAsEmail() {
        CURRENT_STEP = 0;
        step_0.setVisibility(View.VISIBLE);
    }

    private void HideKeyBoard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onBackPressed() {
        if (CURRENT_STEP == 0) {
            super.onBackPressed();
        } else {
            switch (CURRENT_STEP) {
                case 1: { // STEP 1 FROM Facebook
                    super.onBackPressed();
                    break;
                }
                case 2: { //STEP 2
                    CURRENT_STEP = 0;
                    stepsBar.setVisibility(View.GONE);
                    step_2.setVisibility(View.GONE);
                    step_0.setVisibility(View.VISIBLE);
                    break;
                }
                case 3: {
                    CURRENT_STEP = 2;

                    step_3.setVisibility(View.GONE);
                    step_2.setVisibility(View.VISIBLE);
                    break;
                }
                case 4: {
                    CURRENT_STEP = 3;

                    step_4.setVisibility(View.GONE);
                    step_3.setVisibility(View.VISIBLE);
                    break;
                }
                case 5: {
                    //IF BACKPRESSED ON REGISTER SUCCESS -> GO TO LOGIN
                    if (REG_REPLY_CODE == 200) {
                        Intent intent = new Intent(Create_Profile.this, MainActivity.class);
                        startActivity(intent);
                        create_profile.finish();
                        break;
                    }

                }
            }
            ChangeStepBar();
            UpdateStepBar();
        }
    }

    //go to step 2 from email register
    public void btn_Register(View view) {
        // CHECK IF AVATAR IS SELECTED

        //GET USERNAME
        String username = userName.getText().toString();
        //GET EMAIL
        String usermail = userEmail.getText().toString();
        //GET PASSWORD
        String userpass = passWord.getText().toString();


        Boolean isValid = true;
        //TODO do further validations ???
        if (!username.equals("Username") && username != null) {
            User_Data.Username = username;
        } else {
            Toast.makeText(this, "Invalid Username!", Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        if (!usermail.equals("Email") && usermail != null) {
            User_Data.Email = usermail;
        } else {
            Toast.makeText(this, "Invalid Email!", Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        if (!userpass.equals("**********") && userpass != null) {
            User_Data.Password = userpass;
        } else {
            Toast.makeText(this, "Invalid Password!", Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        if (User_Data.Avatar_Img_Link != null) {
            //all fields values ware changed
            // Toast.makeText(this, "Invalid Password!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Please choose a profile picture!", Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        //DO BASIC STRING VALIDATIONS
        if (isValid) {
            //GO TO NEXT STEP
            NextStep(0);
            HideKeyBoard(view);
        } else {
            //Toast.makeText(this, "Invalid Details!", Toast.LENGTH_SHORT).show();
        }
    }

    public void btn_Step1(View view) {
        String username = fb_userName.getText().toString();
        Boolean isValid = true;
        //TODO do further validations ???
        if (!username.equals("Username") && username != null) {
            User_Data.Username = username;
        } else {
            Toast.makeText(this, "Invalid Username!", Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        if (isValid) {
            //GO TO NEXT STEP
            NextStep(1);
            HideKeyBoard(view);
        } else {
            //Toast.makeText(this, "Invalid Details!", Toast.LENGTH_SHORT).show();
        }
    }

    public void btn_Step2(View view) {
        String U_firstName = firstName.getText().toString();
        String U_lastName = lastName.getText().toString();
        String U_birthDate = birthDate.getText().toString();

        Boolean isValid = true;
        //TODO do further validations ???
        if (!U_firstName.equals("First Name") && U_firstName != null) {
            User_Data.FirstName = U_firstName;
        } else {
            Toast.makeText(this, "Invalid First Name!", Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        if (!U_lastName.equals("Last Name") && U_lastName != null) {
            User_Data.LastName = U_lastName;
        } else {
            Toast.makeText(this, "Invalid Last Name!", Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        if (!equals("Birth Date") && U_birthDate != null) {
            User_Data.BirthDate = U_birthDate;
        } else {
            Toast.makeText(this, "Invalid Birth Date!", Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        //DO BASIC STRING VALIDATIONS
        if (isValid) {
            //GO TO NEXT STEP
            NextStep(2);
            HideKeyBoard(view);
        } else {
            //Toast.makeText(this, "Invalid Details!", Toast.LENGTH_SHORT).show();
        }
    }

    public void btn_Step3(View view) {
        //GET PASSWORD
        String UuserAboutU = userAboutU.getText().toString();

        Boolean isValid = true;
        //TODO do further validations ???
        if (!UuserAboutU.equals("About U") && UuserAboutU != null) {
            User_Data.AboutU = UuserAboutU;
        } else {
            Toast.makeText(this, "Invalid About U!", Toast.LENGTH_SHORT).show();
            isValid = false;
        }
        //DO BASIC STRING VALIDATIONS
        if (isValid) {
            //GO TO NEXT STEP
            NextStep(3);
            HideKeyBoard(view);
        }
    }

    public void btn_Step4(View view) {
        // Finaly register the user
//        if (SelectedValues.size() < 3) {
//            Toast.makeText(this, "Please select " + (3 - SelectedValues.size()) + " more interest(s)", Toast.LENGTH_SHORT).show();
//            return;
//        }
        doRegister();
    }

    private void doRegister() {
        DisplayErrorFrame("Registering", "Please Wait");
        if (!IsRegistering) {
            IsRegistering = true;
            insertUser();
        }
    }

    private void DisplayErrorFrame(String topic, String value) {
        CURRENT_STEP = 5;
        if (errorFrame == null) {
            errorFrame = (FrameLayout) create_profile.findViewById(R.id.errorFrame);
            topic_txt = (TextView) create_profile.findViewById(R.id.topic_txt);
            result_txt = (TextView) create_profile.findViewById(R.id.result_txt);
        }
        if (btn_retry.isShown()) {
            btn_retry.setVisibility(View.GONE); //HIDE THE ACTION BUTTON, IF IT WAS LEFT VISIBLE IN LAST MESSAGE
        }
        if (!errorFrame.isShown()) {
            errorFrame.setVisibility(View.VISIBLE);
        }
        //HIDE LAST STEP (step_4)
        if (step_4.isShown()) {
            step_4.setVisibility(View.GONE);
        }
        if (stepsBar.isShown()) {
            stepsBar.setVisibility(View.GONE);
        }
        //SET INFO TEXT VALUES
        topic_txt.setText(topic);
        result_txt.setText(value);

        //DEFINE USERNAME, REAL NAME, AVATAR
        //user name
        TextView err_username = (TextView) create_profile.findViewById(R.id.err_username);
        err_username.setText(User_Data.Username);

        //real name
        TextView err_realname = (TextView) create_profile.findViewById(R.id.err_realname);
        String realName = "(" + User_Data.FirstName + " " + User_Data.LastName + ")";
        err_realname.setText(realName);

        //profile_image
        ImageView err_profile_image = (ImageView) create_profile.findViewById(R.id.err_profile_image);
        err_profile_image.setImageResource(AvatarsList.ImageId[User_Data.hiden_avatar]);
    }

    private void ToggleErrorFrameButtons(Integer action) {
        if (!btn_retry.isShown()) {
            btn_retry.setVisibility(View.VISIBLE);
        }
        retry_code = action;
        switch (action) {
            case 1: {
                //REGISTRATION SUCCESS - GO TO LOGIN (MAINVIEW)
                //CHANGE BUTTON TEXT TO LOGIN
                btn_retry.setText("Login");
                break;
            }
            case 2: {
                //REGISTRATION FAILED - RETRY REGISTERING doRegister(); CHANGE TEXT TO RETRY
                btn_retry.setText("Retry");
                break;
            }
            case 3: {
                //REGISTRATION INVALID DETAILS ALREADY USED (GO TO STEP 2) CHANGE TEXT TO RETRY
                btn_retry.setText("Retry");
                break;
            }
            default: {
                //FAILSAFE CASE CHANGE TEXT TO RETRY
                btn_retry.setText("Retry");
                break;
            }
        }
    }

    public void btn_Retry(View view) {
        IsRegistering = false;
        switch (retry_code) {
            case 1: {
                //REGISTRATION SUCCESS - GO TO LOGIN (MAINVIEW)
                Intent intent = new Intent(Create_Profile.this, MainActivity.class);
                startActivity(intent);
                this.finish();
                break;
            }
            case 2: {
                //REGISTRATION FAILED - RETRY REGISTERING (insertUser(); or doRegister();
                doRegister();
                break;
            }
            case 3: {
                //REGISTRATION INVALID DETAILS ALREADY USED (GO TO STEP 0)
                NextStep(5);
                break;
            }
            default: {
                NextStep(5);
                break;
            }
        }
    }

    private void NextStep(Integer currStep) {
        switch (currStep) {
            case 0: {
                CURRENT_STEP = 2;
                ChangeStepBar();
                stepsBar.setVisibility(View.VISIBLE);
                step_0.setVisibility(View.GONE);
                step_2.setVisibility(View.VISIBLE);
                break;
            }
            case 1: {
                step_1.setVisibility(View.GONE);
                if (User_Data.BirthDate == null || birthDate.equals("Birth Date")) {
                    CURRENT_STEP = 2;
                    UpdateStepBar();
                    //hide first and last name
                    firstName.setVisibility(View.GONE);
                    lastName.setVisibility(View.GONE);
                    step_2.setVisibility(View.VISIBLE);
                    break;
                }
                if (User_Data.AboutU == null || userAboutU.equals("About U")) {
                    CURRENT_STEP = 3;
                    UpdateStepBar();
                    step_3.setVisibility(View.VISIBLE);
                    break;
                } else {
                    CURRENT_STEP = 4;
                    UpdateStepBar();
                    step_4.setVisibility(View.VISIBLE);
                    InitializeCategoriesGrid();
                    break;
                }
            }
            case 2: {
                CURRENT_STEP = 3;
                //ChangeStepBar();
                step_2.setVisibility(View.GONE);
                step_3.setVisibility(View.VISIBLE);
                break;
            }
            case 3: {
                CURRENT_STEP = 4;
                step_3.setVisibility(View.GONE);
                step_4.setVisibility(View.VISIBLE);
                stepsBar.setVisibility(View.VISIBLE);
                InitializeCategoriesGrid();
                break;
            }
            case 4: {

                break;
            }
            case 5: {
                //USERNAME EMAIL ALREADY IN USE - RETRY
                errorFrame.setVisibility(View.GONE);
                CURRENT_STEP = 0;
                step_0.setVisibility(View.VISIBLE);
                break;
            }
        }
        ChangeStepBar();
    }

    private void ChangeStepBar() {
        switch (CURRENT_STEP) {
            case 1: {
                UpdateStepBar();
                currStep.setImageResource(R.drawable.evol_bars_s2);
                break;
            }
            case 2: {
                UpdateStepBar();
                currStep.setImageResource(R.drawable.evol_bars_s2);
                break;
            }
            case 3: {
                UpdateStepBar();
                currStep.setImageResource(R.drawable.evol_bars_s3);
                break;
            }
            case 4: {
                UpdateStepBar();
                currStep.setImageResource(R.drawable.evol_bars_s4);
                break;
            }
            case 0: {
                //hide bar
                currStep.setVisibility(View.GONE);
                break;
            }
        }
    }

    private void UpdateStepBar() {

        if (CURRENT_STEP == 4) {
            private_avatar.setVisibility(View.VISIBLE);
            profile_image.setVisibility(View.INVISIBLE);
        } else {
            private_avatar.setVisibility(View.INVISIBLE);
            profile_image.setVisibility(View.VISIBLE);
        }

        if (CURRENT_STEP == 2) {
            //hide tap to upload cover
            coverUpl_text.setVisibility(View.VISIBLE);
        } else {
            if (coverUpl_text.isShown()) {
                coverUpl_text.setVisibility(View.GONE);
            }
        }
        //Updates Avatar Image
        if (CURRENT_STEP == 1 || is_fb_login) {
            is_fb_login = true;
            if (User_Data.Avatar_Img_Link != null && cached_avatar != User_Data.Avatar_Img_Link) {
                cached_avatar = User_Data.Avatar_Img_Link;
                Picasso.with(this)
                        .load(User_Data.Avatar_Img_Link)
                        .placeholder(R.drawable.ic_placeholder) // optional
                        .error(R.drawable.ic_error_fallback)    // optional
                        .into(avatar);
            }
            if (User_Data.Cover_Img_Link != null && cached_cover != User_Data.Cover_Img_Link) {
                cached_cover = User_Data.Cover_Img_Link;
                Picasso.with(this)
                        .load(User_Data.Cover_Img_Link)
                        .placeholder(R.drawable.ic_placeholder) // optional
                        .error(R.drawable.ic_error_fallback)    // optional
                        .into(cover);
            }
        } else {
            if (User_Data.Avatar_Img_Link != null && cached_avatar != User_Data.Avatar_Img_Link) {
                cached_avatar = User_Data.Avatar_Img_Link;
                Picasso.with(this)
                        .load(new File(User_Data.Avatar_Img_Link))
                        .fit()
                        .placeholder(R.drawable.ic_placeholder) // optional
                        .error(R.drawable.ic_error_fallback)         // optional
                        .into(avatar);
            }

            User_Data.Cover_Img_Link = null;
            if (User_Data.Cover_Img_Link != null && cached_cover != User_Data.Cover_Img_Link) {
                cached_cover = User_Data.Cover_Img_Link;
                Picasso.with(this)
                        .load(new File(User_Data.Cover_Img_Link))
                        .fit()
                        .placeholder(R.drawable.ic_placeholder) // optional
                        .error(R.drawable.ic_error_fallback)    // optional
                        .into(cover);
            }
        }
    }

    private int private_avatar_selected = 0;

    private void InitializeCategoriesGrid() {
        CategoriesGrid adapter = new CategoriesGrid(Create_Profile.create_profile, AvatarsList.ImageId, SelectedValues);
        catGrid = (GridView) findViewById(R.id.catGrid);
        catGrid.setAdapter(adapter);
        catGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                //ITEM NOT FOUND IN THE LIST, SELECT CATEGORIE
                private_avatar_selected = (int) id;
                private_avatar.setImageResource(AvatarsList.ImageId[private_avatar_selected]);
                User_Data.hiden_avatar = private_avatar_selected;

                //Toast.makeText(Create_Profile.create_profile, "You Clicked at: (" + position + "/" + id + ")", Toast.LENGTH_SHORT).show();
                /*
                if (!SelectedValues.contains(id)) {

                    SelectedValues.add(id);
                    ImageView more = (ImageView) view.findViewById(R.id.grid_image);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        more.getBackground().setColorFilter(view.getResources().getColor(R.color.selected_categories), PorterDuff.Mode.SRC_IN);
                        //Toast.makeText(MainActivity.mainactivity, "You Clicked at 1" + AvatarsList.Categories[position] + "(" + position + "/" + id + ")", Toast.LENGTH_SHORT).show();
                    } else {
                        more.getBackground().setColorFilter(view.getResources().getColor(R.color.selected_categories), PorterDuff.Mode.SRC_IN);
                        //Toast.makeText(MainActivity.mainactivity, "You Clicked at 2" + AvatarsList.Categories[position] + "(" + position + "/" + id + ")", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    //ITEM ALREADY IN THE LIST, DESELECT
                    SelectedValues.remove(id);
                    ImageView more = (ImageView) view.findViewById(R.id.grid_image);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        more.getBackground().setColorFilter(view.getResources().getColor(R.color.blueAccent), PorterDuff.Mode.SRC_IN);
                    } else {
                        //Drawable wrapDrawable = DrawableCompat.wrap(more.getBackground());
                        //DrawableCompat.setTint(wrapDrawable, view.getResources().getColor(R.color.blueAccent));
                        //more.setBackgroundDrawable(DrawableCompat.unwrap(wrapDrawable));
                        more.getBackground().setColorFilter(view.getResources().getColor(R.color.selected_categories), PorterDuff.Mode.SRC_IN);
                    }
                }
                */
                //Update user data with new selected values string
                User_Data.SelectedInterests = SelectedValues.toString();
            }
        });
        //HACK, SETTING NUMCOLIMNS ON XML DIDN`T WORK..
        catGrid.setNumColumns(3);


    }

    public void btn_UploadPic(View view) {
        Intent i = new Intent(
                Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        UploadAction = "profile_upload";
        startActivityForResult(i, RESULT_LOAD_IMAGE);
    }

    public void btn_UploadCov(View view) {
        if (CURRENT_STEP == 2) {
            Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            UploadAction = "cover_upload";
            startActivityForResult(i, RESULT_LOAD_IMAGE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
            }

            if (UploadAction.equals("profile_upload")) {
                Uri selectedImageUri = data.getData();
                User_Data.Avatar_Img_Link = getPath(selectedImageUri);
                Picasso.with(this)
                        .load(selectedImageUri)
                        .fit()
                        .placeholder(R.drawable.ic_placeholder) // optional
                        .error(R.drawable.ic_error_fallback)         // optional
                        .into(profile_upload);
            }
            if (UploadAction.equals("cover_upload")) {
                Uri selectedImageUri = data.getData();
                User_Data.Cover_Img_Link = getPath(selectedImageUri);
                Picasso.with(this)
                        .load(selectedImageUri)
                        .fit()
                        .placeholder(R.drawable.ic_placeholder) // optional
                        .error(R.drawable.ic_error_fallback)         // optional
                        .into(cover);
            }
            cursor.close();
            // String picturePath contains the path of selected Image
        }
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public void insertUser() {
        //Here we will handle the http request to register the user to mysql db
        Kryptonite krypton = new Kryptonite();
        User krypton_User = new User();
        //SEND STRINGS TO KRYPTON
        krypton_User.Username = Kryptonite.bytesToHex((krypton.encrypt(User_Data.Username)));
        krypton_User.Email = Kryptonite.bytesToHex((krypton.encrypt(User_Data.Email)));
        if (is_fb_login) {
            krypton_User.Password = Kryptonite.bytesToHex(krypton.encrypt(User_Data.FacebookID));
        } else {
            krypton_User.Password = Kryptonite.bytesToHex(krypton.encrypt(User_Data.Password));
        }
        krypton_User.FirstName = Kryptonite.bytesToHex((krypton.encrypt(User_Data.FirstName)));
        krypton_User.LastName = Kryptonite.bytesToHex((krypton.encrypt(User_Data.LastName)));
        krypton_User.BirthDate = Kryptonite.bytesToHex((krypton.encrypt(User_Data.BirthDate)));
        krypton_User.AboutU = Kryptonite.bytesToHex(krypton.encrypt(User_Data.AboutU));
        krypton_User.SelectedInterests = Kryptonite.bytesToHex(krypton.encrypt(User_Data.SelectedInterests));
        krypton_User.FacebookID = Kryptonite.bytesToHex(krypton.encrypt(User_Data.FacebookID));
        krypton_User.hiden_avatar = User_Data.hiden_avatar;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        String cryted_Action = Kryptonite.bytesToHex(krypton.encrypt("register_user"));
        Log.e("KRYPTON-ACTION", cryted_Action);


        //GET GCM TOKEN
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        String SENDER_ID = MainActivity.mainactivity.GCM_SENDER_ID;
        String regid = null;
        regid = Kryptonite.bytesToHex(krypton.encrypt(InstanceID.getInstance(this).getId()));

        String token = new StorePrefs().GetGCMInstanceID(this);

        String cryted_Token = Kryptonite.bytesToHex(krypton.encrypt(token));

        RegisterUserAPI service = retrofit.create(RegisterUserAPI.class);
        final Call<RegisterUser> call = service.regsUser(
                cryted_Action,
                krypton_User.Username,
                krypton_User.Email,
                krypton_User.Password,
                krypton_User.FirstName,
                krypton_User.LastName,
                krypton_User.BirthDate,
                krypton_User.AboutU,
                krypton_User.SelectedInterests,
                User_Data.isFacebook,
                krypton_User.FacebookID,
                cryted_Token,
                krypton_User.hiden_avatar);

        Log.e("KRYPTON",
                "username: " + krypton_User.Username +
                        " email: " + krypton_User.Email +
                        " password: " + krypton_User.Password +
                        " firstName: " + krypton_User.FirstName +
                        " lastName: " + krypton_User.LastName +
                        " BirthDate: " + krypton_User.BirthDate +
                        " AboutU: " + krypton_User.AboutU +
                        " Ints: " + krypton_User.SelectedInterests +
                        " isFB: " + User_Data.isFacebook +
                        " fb_id: " + krypton_User.FacebookID +
                        " token: " + cryted_Token);

        call.enqueue(new Callback<RegisterUser>() {
            @Override
            public void onResponse(Response<RegisterUser> response) {
                Log.e("Response1 code ", String.valueOf(response.code()));
                REG_REPLY_CODE = response.code();
                if (String.valueOf(response.code()).equals("500")) {
                    Toast.makeText(MainActivity.mainactivity, "Registration Failed : " + String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                    return;
                }
                RegisterUser result = response.body();
                String value = response.body().getResult();
                Log.e("KRYPTON-RESULT", "ENCODED BODY: " + response.body().result + " MSG: " + value);

                //IF VALUE IS NULL BREAK THE FUNCTION
                if (value == null) {
                    Log.e("KRYPTON-RESULT", "NO ACTION SET:EXIT " + value);
                    DisplayErrorFrame("ERROR 11", result.getPrint_message());
                    return;
                }

                switch (value) {
                    case "success": {
                        //REGISTRATION SUCCESS
                        RegisterMsg = result.getPrint_message();
                        DisplayErrorFrame("Please Wait", "Uploading Images");
                        Log.e("KRYPTON-RESULT", "REGISTRATION SUCCESS " + RegisterMsg + " ID: " + result.getUserID());
                        //TODO IF UPLOAD FAILS, ASK FOR RETRY OR LOGIN
                        String avatar_filePath = User_Data.Avatar_Img_Link;
                        String cover_filePath = User_Data.Cover_Img_Link;
                        User_Data.UserID = result.getUserID();
                        //COMPRESS IMAGES SIZES
                        //File avatar_file = new File(ImagesCompress.compressImage(avatar_filePath));
                        //File cover_file = new File(ImagesCompress.compressImage(cover_filePath));
                        if (!is_fb_login) {
                            new UploadImage().execute(avatar_filePath, "avatar_upload", User_Data.UserID.toString());
                            new UploadImage().execute(cover_filePath, "cover_upload", User_Data.UserID.toString());
                            mHandler.postDelayed(VerifyUploads, 1000);
                        } else {
                            //is facebook
                            uploaded_avatar = true;
                            uploaded_cover = true;
                            mHandler.postDelayed(VerifyUploads, 500);
                        }
                        break;
                    }
                    case "failed": {
                        //REGISTRATION FAILED FOR SOME REASON
                        Log.e("KRYPTON-RESULT", "REGISTRATION FAILED " + value);
                        DisplayErrorFrame("REGISTRATION FAILED", result.getPrint_message());
                        ToggleErrorFrameButtons(2);
                        break;
                    }
                    case "invalid": {
                        //DETAILS ALREADY IN USE OR DATA NOT SENT COMPLETE
                        Log.e("KRYPTON-RESULT", "REGISTRATION FAILED " + value);
                        DisplayErrorFrame("REGISTRATION FAILED", result.getPrint_message());
                        ToggleErrorFrameButtons(3);
                        break;
                    }
                    case "no_action": {
                        //THE SERVER DIDNT HANDLED THE ACTION: RETRY
                        Log.e("KRYPTON-RESULT", "NO ACTION DONE " + value + " retry : " + faild_count);
                        DisplayErrorFrame("ERROR 15", result.getPrint_message());
                        //FAIL SAFE CODE FOR ISSUE #8
                        if (faild_count < 5) {
                            faild_count++;
                            insertUser();
                        } else {
                            Log.e("KRYPTON-RESULT", "TOTAL DISASTER " + value);
                            DisplayErrorFrame("ERROR 500", "Something went wrong!");
                            ToggleErrorFrameButtons(2);
                        }
                        break;
                    }
                    default: {
                        Log.e("KRYPTON-RESULT", value + "DEFAULT REGISTRATION MSG: " + result.getPrint_message());
                        DisplayErrorFrame("ERROR 35", result.getPrint_message());
                        ToggleErrorFrameButtons(2);
                        break;
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("Failure Response id: ", String.valueOf(t));
                System.out.println(t.getMessage());
                System.out.println(t.getCause());
                DisplayErrorFrame("REGISTRATION FAILURE", "Please retry again later!");
                ToggleErrorFrameButtons(2);
                //FAIL SAFE CODE FOR RESOLVED BUG #8
                /*
                if (faild_count < 3) {
                    faild_count++;
                    insertUser();
                }
                */
            }
        });
        //cover
        //avatar
    }

    public void Avatar_UploadFinished() {
        uploaded_avatar = true;
    }

    public void Cover_UploadFinished() {
        uploaded_cover = true;
    }

}
