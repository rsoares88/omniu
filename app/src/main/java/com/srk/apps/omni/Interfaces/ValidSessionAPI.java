package com.srk.apps.omni.Interfaces;

import com.srk.apps.omni.SyncService.ValidSession;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * OMNI
 * by SRK on 26-01-2016 @ 12:19
 *
 * @SYNAPPS 2016
 */
public interface ValidSessionAPI {
    @GET("AuthUser.php?action=action&sessionID=sessionID&deviceID=deviceID")
    Call<ValidSession> getResult(
            @Query("action") String action,
            @Query("sessionID") String sessionID,
            @Query("deviceID") String deviceID);
}
