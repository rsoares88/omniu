package com.srk.apps.omni.Interfaces;

import com.srk.apps.omni.SyncService.RegisterUser;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * OMNI
 * by SRK on 23-01-2016 @ 17:47
 *
 * @SYNAPPS 2016
 */
public interface RegisterUserAPI {
    @GET("AuthUser.php?action=action" +
            "&username=username" +
            "&email=email" +
            "&password=password" +
            "&firstname=firstname" +
            "&lastname=lastname" +
            "&birthdate=birthdate" +
            "&aboutu=aboutu" +
            "&selectedints=selectedints" +
            "&isFacebook=isFacebook" +
            "&fb_id=fb_id" +
            "&regId=regId" +
            "&selAvt=selAvt")

    Call<RegisterUser> regsUser(
            @Query("action") String action,
            @Query("username") String username,
            @Query("email") String email,
            @Query("password") String password,

            @Query("firstname") String firstname,
            @Query("lastname") String lastname,
            @Query("birthdate") String birthdate,

            @Query("aboutu") String aboutu,

            @Query("selectedints") String selectedints,
            @Query("isFacebook") Boolean isFacebook,
            @Query("fb_id") String fb_id,
            @Query("regId") String regId,
            @Query("selAvt") Integer selAvt

    );

}
