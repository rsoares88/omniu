package com.srk.apps.omni.StorageClass;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.google.gson.Gson;
import com.srk.apps.omni.Activitys.AppView;
import com.srk.apps.omni.Fragments.ChatWindow;
import com.srk.apps.omni.Helpers.ChatArrayAdapter;
import com.srk.apps.omni.Helpers.NotificationsManager;
import com.srk.apps.omni.Krypton.Kryptonite;

/**
 * OMNI
 * by SRK on 26-01-2016 @ 01:20
 *
 * @SYNAPPS 2016
 */
public class StorePrefs {

    String PREFS_NAME = "OMSTR_DATA";
    SharedPreferences StoredUserData;
    /*
    public StorePrefs(Context ctx) {
        LoadSharedPrefs(ctx);
    }
    */
    public StorePrefs() {

    }

    private void LoadSharedPrefs(Context ctx) {
        if (StoredUserData == null && ctx != null ) {
            StoredUserData = ctx.getSharedPreferences(PREFS_NAME, 0);
        }
    }

    public void ResetSharedPrefs(Context ctx) {
        LoadSharedPrefs(ctx);
        SharedPreferences settings = StoredUserData;
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.commit();
    }

    Integer retrys = 0;
    public void StoreUserValues(User user, Context ctx) {
        LoadSharedPrefs(ctx);
        if (StoredUserData == null && ctx != null ) {
            LoadSharedPrefs(ctx);
            retrys++;
            if(retrys > 5) {

                return;
            }
        } else {
            Log.e("STORE-PREFS", "FAILED TO STORE USER PREFS CTX: " + ctx.toString() + " LSP: " + StoredUserData.toString());
            //GET A NET CTX

        }


        Gson gson = new Gson();
        String json = gson.toJson(user);

        Kryptonite krypton = new Kryptonite();

        String krypton_data = Kryptonite.bytesToHex(krypton.encrypt(json));

        SharedPreferences.Editor editor = StoredUserData.edit();
        editor.putString("UserData", krypton_data);
        editor.commit();
        //User KeepLogged = StoredUserData.getS("KeepLogged", false);
        //boolean KeepLogged = StoredUserData.getBoolean("KeepLogged", false);
    }

    public User LoadUserPrefs(Context ctx) {
        LoadSharedPrefs(ctx);

        Kryptonite krypton = new Kryptonite();

        Gson gson = new Gson();
        String json = null;

        if (StoredUserData == null) {
            return new User();
        }

        try {
            if (StoredUserData.getString("UserData", null) != null) {
                json = krypton.decrypt(StoredUserData.getString("UserData", ""));
                User user = gson.fromJson(json, User.class);
                return user;
            } else {
                return new User();
            }


        } catch (Exception e) {
            e.printStackTrace();
            return new User();
        }
    }

    public void SaveGCMInstanceID(String regId, int appVers, Context ctx) {
        LoadSharedPrefs(ctx);
        SharedPreferences.Editor editor = StoredUserData.edit();
        editor.putString("GCMInstanceID", regId);
        editor.putInt("appVers", appVers);
        editor.commit();
    }

    public String GetGCMInstanceID(Context ctx) {
        LoadSharedPrefs(ctx);
        return StoredUserData.getString("GCMInstanceID", null);
    }

    public int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    public int getSoredAppVersion(Context ctx) {
        LoadSharedPrefs(ctx);
        return StoredUserData.getInt("appVers", 0);
    }

    public boolean MatchVersions(Context ctx) {

        int storedVersion = getSoredAppVersion(ctx);
        int currentVersion = getAppVersion(ctx);

        if (storedVersion != currentVersion) {
            return false;
        } else {
            return true;
        }
    }

    public StoredMessages LoadSavedMessages(Context ctx) {
        LoadSharedPrefs(ctx);

        Gson gson = new Gson();
        String json_obj = StoredUserData.getString("StoredMessages", "");
        StoredMessages obj = gson.fromJson(json_obj, StoredMessages.class);

        return obj;
    }

    public void addStoredMessage(Context ctx, int fromID, String from_username, String from_aboutU, Integer isFriends, ChatMessage message) {
        StoredMessages current_messages = this.LoadSavedMessages(ctx);

        Log.e("GOT-PUSH", "USERNAME: " + from_username + "UID: " + message.uniqueID);
        StoreMessage new_message = new StoreMessage();
        new_message.from_ID = fromID;
        new_message.from_username = from_username;
        new_message.from_aboutU = from_aboutU;
        new_message.isFriends = isFriends;
        new_message.messages.add(message);

        if (current_messages == null) {
            //no messages found create new stored_messages
            current_messages = new StoredMessages();
            current_messages.MessagesStored.add(new_message);
            StoreSavedMessages(ctx, current_messages);
            return;
        } else {
            //check if user already have messages from this user
            for (StoreMessage item : current_messages.MessagesStored) {
                if(item.from_ID == fromID) {
                    item.messages.add(message);
                    StoreSavedMessages(ctx, current_messages);
                    return;
                }
            }
            //if i reach here the StoreMessages didnt had msg from this ID
            current_messages.MessagesStored.add(new_message);
            StoreSavedMessages(ctx, current_messages);
            return;
        }
    }

    public void delStoredMessage(Context ctx, int fromID, String msgID) {
        LoadSharedPrefs(ctx);
        StoredMessages current_messages = this.LoadSavedMessages(ctx);
        for (StoreMessage item : current_messages.MessagesStored) {
           // if (item.from_ID == fromID || item.from_ID == AppView.instance.userdata.UserID) {
                // this item contains the message i want to delete
                for (ChatMessage message : item.messages) {
                    Log.e("MSG-DEL", message.uniqueID + " / " + msgID);
                    if (message.uniqueID.equals(msgID)) {
                        //message is the message i want to remove from list
                        Log.e("MSG-DEL", message.uniqueID + " DELETED: " + item.messages.contains(message) + " index: " + item.messages.indexOf(message));
                        item.messages.remove(item.messages.indexOf(message));
                        //SAVE STORED MESSAGED AGAIN
                        break;
                        //return;
                    }
                }
           // }
        }
        StoreSavedMessages(ctx, current_messages);
        Log.e("MSG-DEL", "NO MESSAGE FOUND TO DELETE");
    }

    public StoreMessage getMessagesFromID(Context ctx, int fromID) {
        LoadSharedPrefs(ctx);
        StoredMessages current_messages = this.LoadSavedMessages(ctx);

        if(current_messages == null) {
            return null;
        }

        for (StoreMessage item : current_messages.MessagesStored) {
            if (item.from_ID == fromID) {
                Log.e("MSG-GET", "GOT: " + item.messages.size());
                return item;
            }
        }
        return null;
    }

    public void updateMessageCounter(Context ctx, Integer fromID, String uID) {
        LoadSharedPrefs(ctx);
        StoredMessages current_messages = this.LoadSavedMessages(ctx);

        if (current_messages == null) {
            ChatWindow.instance.toggleUserDetails(true);
            return;
        }

        ChatWindow.instance.toggleUserDetails(false);
        for (StoreMessage item : current_messages.MessagesStored) {
            Log.e("DEBUG", "READING MSGS FOR ID: " + item.from_ID + " / " + fromID);
            //if (item.from_ID == fromID || item.from_ID == AppView.instance.userdata.UserID) {
                for (ChatMessage msg : item.messages) {
                    Log.e("DEBUG", "FROM: " + item.from_ID + "CURRENT uID: " + msg.uniqueID + " / " + uID);
                    if (msg.uniqueID.equals(uID)) {
                        msg.readTime++;
                        if (msg.readTime >= ChatArrayAdapter.max_interval) {
                            delStoredMessage(ctx, fromID, uID);
                            break;
                        } else {
                            StoreSavedMessages(ctx, current_messages);
                        }

                        Log.e("STR-UPD", "MESSAGE: " + msg.uniqueID + "READTIMER UPDATED!");
                        break;
                    }
                }
            //}
        }
        Log.e("STR-UPD", "NO MESSAGE FOUND TO UPDATE: " + uID);
        return;
    }

    private void StoreSavedMessages(Context ctx, StoredMessages toStoreList) {
        LoadSharedPrefs(ctx);
        SharedPreferences.Editor editor = StoredUserData.edit();
        editor.remove("StoredMessages");
        Gson gson = new Gson();
        String json = gson.toJson(toStoreList);
        editor.putString("StoredMessages", json);
        editor.commit();
    }

    public boolean ifMessageExists(Context ctx, Integer fromID, String uniqueID) {
        StoredMessages current_messages = this.LoadSavedMessages(ctx);
        if(current_messages ==  null) {
            return false;
        }
        //ELSE CHECK IF MESSAGE EXISTS
        for (StoreMessage item : current_messages.MessagesStored) {
            if (item.from_ID == fromID) {
                for (ChatMessage messages : item.messages) {
                    if (messages.uniqueID == uniqueID) {
                        return true;
                    }
                }

            }
        }
        return false;
    }

    public void AddNotifications(Context ctx, NotificationItem item) {
        NotificationsManager notifications = LoadNotifications(ctx);

        Log.e("DEBUG", "HAS ITEM:  " + item.Flag);
        if(notifications == null) {
            notifications = new NotificationsManager();
        }
        notifications.addItem(item);
        SaveNotification(ctx, notifications);
    }

    public void RemoveNotification(Context ctx, String uniqueID) {
        NotificationsManager notifications = LoadNotifications(ctx);
        for (NotificationItem item :  notifications.NotificationsList)
        {
            if(item.UniqueID.equals(uniqueID)) {
                //this item exists in notification manager
                 // remove
                notifications.NotificationsList.remove(item);
                SaveNotification(ctx, notifications);
                //TODO DEBUG CODE
                NotificationsManager nEWnotifications = LoadNotifications(ctx);
                Log.e("DEBUG", "REMOVED NOTF: " + uniqueID + "COUNT: " + nEWnotifications.NotificationsList.size());

                break;
            }
        }


    }

    public NotificationsManager LoadNotifications(Context ctx) {
        LoadSharedPrefs(ctx);
        if(StoredUserData == null) {

        }
        Gson gson = new Gson();
        String json_obj = StoredUserData.getString("NotificationsList", "");
        if(json_obj == null) {
            Log.e("STR.GSON", "INVALID JSON");
        }
        NotificationsManager obj = gson.fromJson(json_obj, com.srk.apps.omni.Helpers.NotificationsManager.class);

         return obj;
    }

    public void SaveNotification(Context ctx, NotificationsManager items) {
        SharedPreferences.Editor editor = StoredUserData.edit();
        editor.remove("NotificationsList");
        Gson gson = new Gson();
        String json = gson.toJson(items);
        editor.putString("NotificationsList", json);
        editor.commit();

        if(AppView.instance != null) {
            AppView.instance.UpdateNotifications();
        }
    }

}
