package com.srk.apps.omni.SyncService;

import android.util.Log;

import com.srk.apps.omni.Krypton.Kryptonite;

/**
 * OMNI
 * by SRK on 23-01-2016 @ 17:29
 *
 * @SYNAPPS 2016
 */
public class RegisterUser {

    public String action;
    public String result;
    public String print_message;
    public String userID;

    public RegisterUser(String action,
                        String result,
                        String print_message) throws Exception {
        Log.e("KRYPTON-REGS", "DECODED RESULT = " + action.toString());
    }

    public String getResult() {
        try {
            Kryptonite mcrypt = new Kryptonite();
            String decrypted = mcrypt.decrypt(result);
            return decrypted;
        } catch (Exception e) {
            Log.e("KRYPTON", "getResult FAILED TO DECODE RESULT = " + result);
            e.printStackTrace();
            return result;
        }
    }

    public String getPrint_message() {
        try {
            Kryptonite mcrypt = new Kryptonite();
            String decrypted = mcrypt.decrypt(print_message);
            return decrypted;
        } catch (Exception e) {
            Log.e("KRYPTON", "getPrint_message FAILED TO DECODE RESULT = " + print_message);
            e.printStackTrace();
            return print_message;
        }
    }

    public Integer getUserID() {
        try {
            Kryptonite mcrypt = new Kryptonite();
            Integer decrypted = Integer.valueOf(mcrypt.decrypt(userID));
            return decrypted;
        } catch (Exception e) {
            Log.e("KRYPTON", "getUserID FAILED TO DECODE RESULT = " + userID);
            e.printStackTrace();
            return Integer.valueOf(userID);
        }
    }

}
