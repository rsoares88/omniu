package com.srk.apps.omni.SyncService;

import android.util.Log;

import com.srk.apps.omni.Krypton.Kryptonite;

/**
 * OMNI
 * by SRK on 26-01-2016 @ 12:20
 *
 * @SYNAPPS 2016
 */
public class ValidSession {

    public String result;

    private String getResultString() {
        try {
            Kryptonite mcrypt = new Kryptonite();
            String decrypted = mcrypt.decrypt(result);
            return decrypted;
        } catch (Exception e) {
            Log.e("KRYPTON", "FAILED TO DECODE ValidSession = " + result);
            e.printStackTrace();
            return result;
        }
    }

    public Boolean getResult() {
        String value = getResultString();
        Log.e("KRYPTON", "session getResult: " + value);
        if(value.equals("true")) {
            return true;
        } else {
            return false;
        }
    }
}
