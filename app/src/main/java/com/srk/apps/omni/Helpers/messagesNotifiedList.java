package com.srk.apps.omni.Helpers;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.srk.apps.omni.Activitys.AppView;
import com.srk.apps.omni.R;
import com.srk.apps.omni.StorageClass.NotificationItem;
import com.srk.apps.omni.StorageClass.StorePrefs;

import java.util.List;

/**
 * OMNI
 * by SRK on 29-02-2016 @ 15:24
 *
 * @SYNAPPS 2016
 */
public class messagesNotifiedList  extends ArrayAdapter<NotificationItem> {

    Context context;
    public static messagesNotifiedList instance;
    private List<NotificationItem> NotificationsList;

    public messagesNotifiedList(Context context, int resourceId, //resourceId=your layout
                            List<NotificationItem> items) {
        super(context, resourceId, items);
        this.context = context;
        this.instance = this;
        this.NotificationsList = items;
    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView icon;
        TextView firstLine;
        TextView secondLine;
        ImageView openOptions;

        RelativeLayout itemList;
        RelativeLayout moreOptions;

        ImageView option1;
        ImageView option2;
    }

    public Integer getUserIndex(String fromUserID) {

        for (NotificationItem item : this.NotificationsList) {
            if(item.UniqueID == fromUserID) {
                return this.NotificationsList.indexOf(item);
            }
        }
        return -1;
    }

    protected int MOVE_FACTOR = 5;

    private final int OPTIONS_WIDTH = 192;
    private final float OPTIONS_MARGIN_LEFT = (float)(OPTIONS_WIDTH / 3.5);
    private final float OPTIONS_MARGIN_RIGTH = (float)(OPTIONS_WIDTH / 1.1);

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        final NotificationItem rowItem = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.scaned_users_list, null);
            holder = new ViewHolder();
            holder.firstLine = (TextView) convertView.findViewById(R.id.firstLine);
            holder.secondLine = (TextView) convertView.findViewById(R.id.secondLine);
            holder.icon = (ImageView) convertView.findViewById(R.id.icon);
            holder.openOptions = (ImageView) convertView.findViewById(R.id.openOptions);
            holder.moreOptions = (RelativeLayout) convertView.findViewById(R.id.moreOptions);
            holder.itemList = (RelativeLayout) convertView.findViewById(R.id.itemList);
            holder.option1 = (ImageView) convertView.findViewById(R.id.view_profile);
            holder.option2 = (ImageView) convertView.findViewById(R.id.chat_user);

            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
        //Log.e("LIST", "USERNAME: " + rowItem.username + " ABOUTU: " + rowItem.description);
        holder.firstLine.setText(rowItem.Title);
        holder.secondLine.setText(rowItem.Desc);
        holder.icon.setImageResource(R.drawable.private_avatar);

        //more options click listner
        final ViewHolder finalHolder = holder;
        holder.openOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Integer width = finalHolder.moreOptions.getLayoutParams().width;
                Log.e("CLICKED INSIDE", "" + position + " with width: " + width);
                //TODO WHILE IS TO FAST MOVE TO HANDLER
                if (width <= 0) {
                    //open view moreoptions
                    OpenPanel(finalHolder);
                } else {
                    //close view moreoptions
                    ClosePanel(finalHolder);
                }

            }
        });

        //options click listeners
        holder.option1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Log.e("CLICKED OPTION 1", "" + position);
                StorePrefs storage = new StorePrefs();
                String Reset_AboutU = rowItem.Desc;

                rowItem.Desc = Reset_AboutU;
                //AppView.instance.OpenViewUserProfile(rowItem);
            }
        });

        holder.option2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Log.e("CLICKED OPTION 2", "" + position);
                StorePrefs storage = new StorePrefs();
                String Reset_AboutU = rowItem.Desc;
                if (storage.getMessagesFromID(AppView.instance, rowItem.fromUserID) != null) {
                    Reset_AboutU = storage.getMessagesFromID(AppView.instance, rowItem.fromUserID).from_aboutU;
                }
                rowItem.Desc = Reset_AboutU;
                AppView.instance.OpenUserChatWindow(rowItem);
            }
        });

        //more options Click Listners
        holder.moreOptions.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                //Log.e("CLICKED MOREOPTIONS", "" + position);
            }
        });

        holder.itemList.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Log.e("CLICKED NOTIFIC", "" + position + "userID: " + rowItem.UniqueID + " FLAG: " + rowItem.Flag);
                StorePrefs storage = new StorePrefs();
                String Reset_AboutU = rowItem.Desc;
                if (storage.getMessagesFromID(AppView.instance, rowItem.fromUserID) != null) {
                Reset_AboutU = storage.getMessagesFromID(AppView.instance, rowItem.fromUserID).from_aboutU;
                }
                rowItem.Desc = Reset_AboutU;

                switch (rowItem.Flag) {
                    case "friend_request": {
                        String username_from = storage.getMessagesFromID(AppView.instance, rowItem.fromUserID).from_username;
                        AppView.instance.friendRequestDialog(username_from, rowItem.fromUserID);
                    break;
                    }
                    case "chat_window": {
                        AppView.instance.OpenUserChatWindow(rowItem);
                    break;
                    }
                }
            }
        });

        holder.itemList.setOnTouchListener(
                new View.OnTouchListener() {

                    int padding;
                    float x1, x2;

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction()) {
                            case MotionEvent.ACTION_CANCEL: {
                                Log.e("BOUDS", "OUT OF BOUNDS CANCEL");
                                long downTime = SystemClock.uptimeMillis();
                                long eventTime = SystemClock.uptimeMillis() + 100;
                                float x = x1;
                                float y = x2;
                                int metaState = 0;
                                MotionEvent motionEvent_UP = MotionEvent.obtain(
                                        downTime,
                                        eventTime,
                                        MotionEvent.ACTION_UP,
                                        x,
                                        y,
                                        metaState
                                );
                                onTouch(v, motionEvent_UP);
                                break;
                            }

                            case MotionEvent.ACTION_MOVE: {
                                float current_X = event.getX();
                                padding = (int) ((current_X - x1));
                                Integer width = finalHolder.moreOptions.getLayoutParams().width;
                                if (padding == 0) {
                                    break;
                                }
                                //Log.d("SWIPE", "MOVE swipe, CUR-LEFTPAD: " + v.getPaddingLeft() + " PAD-MAX: "
                                //      + finalHolder.moreOptions.getLayoutParams().width + "PAD-VAL: " + padding);
                                //SWIPE LEFT
                                if (padding < 0 && width < OPTIONS_WIDTH) {
                                    int padder = v.getPaddingLeft() - MOVE_FACTOR;
                                    v.setPadding(padder, 0, 0, 0);
                                    finalHolder.moreOptions.getLayoutParams().width += MOVE_FACTOR;
                                    UpdateArrows(finalHolder);
                                    break;
                                }
                                //SWIPE RIGTH
                                if (padding > 0 && width > 0) {
                                    int padder = v.getPaddingLeft() + MOVE_FACTOR;
                                    v.setPadding(padder, 0, 0, 0);
                                    finalHolder.moreOptions.getLayoutParams().width -= MOVE_FACTOR;
                                    UpdateArrows(finalHolder);
                                    break;
                                }
                                break;
                            }

                            case MotionEvent.ACTION_DOWN: {
                                x1 = event.getX();
                                break;
                            }
                            case MotionEvent.ACTION_UP: {
                                x2 = event.getX();
                                if (padding == 0) {
                                    finalHolder.itemList.performClick();
                                    break;
                                }

                                Integer width = finalHolder.moreOptions.getLayoutParams().width;

                                if (padding < 0 && width >= (OPTIONS_MARGIN_LEFT)) {
                                    if (width >= OPTIONS_WIDTH) {
                                        break;
                                    }
                                    //finish moving lef
                                    OpenPanel(finalHolder);
                                    return true;
                                } else if (padding < 0 && width <= (OPTIONS_MARGIN_LEFT)) {
                                    //didnt move enough go back to right
                                    if (width <= 0) {
                                        break;
                                    }
                                    ClosePanel(finalHolder);
                                    return true;
                                }

                                if (padding > 0 && width < (OPTIONS_MARGIN_RIGTH)) {
                                    //finish moving right
                                    if (width <= 0) {
                                        break;
                                    }
                                    //Log.e("DRAG", "ACTION_UP MID WAY FINISH MOVING RIGHT");
                                    ClosePanel(finalHolder);
                                    return true;
                                } else if (padding > 0 && width < OPTIONS_WIDTH) {
                                    //didnt move enough, move back to left
                                    if (width >= OPTIONS_WIDTH) {
                                        break;
                                    }
                                    //Log.e("DRAG", "ACTION_UP DIDNT MOVE ENOUGH, CLOSE BACK");
                                    OpenPanel(finalHolder);
                                    return true;
                                }

                            }
                        }
                        return true;
                    }
                });

        return convertView;
    }

    ValueAnimator animation_a;
    ValueAnimator animation_b;

    void OpenPanel(final ViewHolder finalHolder) {
        animation_a = ValueAnimator.ofInt(finalHolder.itemList.getPaddingLeft(), -OPTIONS_WIDTH);
        animation_a.setDuration(500);
        animation_a.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int value = Integer.parseInt(valueAnimator.getAnimatedValue().toString());
                finalHolder.itemList.setPadding(value, 0, 0, 0);
            }
        });
        if (!animation_a.isRunning()) {
            animation_a.start();
        }

        animation_b = ValueAnimator.ofInt(finalHolder.moreOptions.getLayoutParams().width, OPTIONS_WIDTH);
        animation_b.setDuration(500);
        animation_b.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int value = Integer.parseInt(valueAnimator.getAnimatedValue().toString());
                finalHolder.moreOptions.getLayoutParams().width = value;
                UpdateArrows(finalHolder);
                //Log.e("OPEN anim_A", "" + value);
            }
        });
        if (!animation_b.isRunning()) {
            animation_b.start();
        }
    }

    void ClosePanel(final ViewHolder finalHolder) {
        animation_a = ValueAnimator.ofInt(finalHolder.itemList.getPaddingLeft(), 0);
        animation_a.setDuration(500);
        animation_a.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int value = Integer.parseInt(valueAnimator.getAnimatedValue().toString());
                finalHolder.itemList.setPadding(value, 0, 0, 0);
                //Log.e("CLOSE anim_A", "" + value);
            }
        });
        if (!animation_a.isRunning()) {
            animation_a.start();
        }

        animation_b = ValueAnimator.ofInt(finalHolder.moreOptions.getLayoutParams().width, 0);
        animation_b.setDuration(500);
        animation_b.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int value = Integer.parseInt(valueAnimator.getAnimatedValue().toString());
                finalHolder.moreOptions.getLayoutParams().width = value;
                UpdateArrows(finalHolder);
            }
        });
        if (!animation_b.isRunning()) {
            animation_b.start();
        }
    }

    void UpdateArrows(ViewHolder finalHolder) {
        Integer width = finalHolder.moreOptions.getLayoutParams().width;
        //CHANGE ARROW POSITION
        //BREAK IF WIDTH IS 0 OR >= MAX
        if(width >= OPTIONS_WIDTH ) {
            //change arrow to close
            finalHolder.openOptions.setImageResource(R.drawable.dw_arrow_close);
        }
        if(width <= 0) {
            //change arrow to open
            finalHolder.openOptions.setImageResource(R.drawable.dw_arrow_open);
        }
    }


}
