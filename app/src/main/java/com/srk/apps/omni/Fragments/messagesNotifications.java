package com.srk.apps.omni.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.srk.apps.omni.Helpers.NotificationsManager;
import com.srk.apps.omni.Helpers.messagesNotifiedList;
import com.srk.apps.omni.R;

/**
 * OMNI
 * by SRK on 29-02-2016 @ 13:42
 *
 * @SYNAPPS 2016
 */
public class messagesNotifications extends Fragment {


    public messagesNotifications() {
        // Required empty public constructor
    }


    View view;
    public static messagesNotifications instance;

    ListView listView;

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                       Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        instance = this;
        view = inflater.inflate(R.layout.fragment_messages_notifications, container, false);
        return view;
    }

    @Override public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //activity created, do something
        Bundle args = getArguments();
        NotificationsManager rawValues;
        Integer userID;
        if (args != null) {
            rawValues = (NotificationsManager)args.getSerializable("NotifiedList");
            userID = args.getInt("userID");
        } else return;
        Log.e("USERS-RES", "FINISHED OPENING Notifications List: " + rawValues + " MYID: " + userID);

        //get messages list

        listView = (ListView) view.findViewById(R.id.listview);
        messagesNotifiedList adapter = new messagesNotifiedList(view.getContext(),
                R.layout.scaned_users_list, rawValues.NotificationsList);
        listView.setAdapter(adapter);

    }

/*
    @Override public void onListItemClick(ListView l, View v, int position, long id) {
        // TODO implement some logic
    }
*/



}
