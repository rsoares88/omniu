package com.srk.apps.omni.StorageClass;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * OMNI
 * by SRK on 08-02-2016 @ 13:29
 *
 * @SYNAPPS 2016
 */
public class StoreMessage implements Serializable {

    public Integer from_ID;
    public String from_username;
    public String from_aboutU;
    public Integer isFriends = 0;

    public String flag = "chat_window";

    public String user_avatar = "null";
    public String user_cover = "null";

    public List<ChatMessage> messages = new ArrayList<>();


    public ChatMessage getLastMsg() {
        return messages != null && !messages.isEmpty() ? messages.get(messages.size() - 1) : null;
    }

    public ChatMessage getFirstMsg() {
        return messages != null && !messages.isEmpty() ? messages.get(0) : null;
    }

}
