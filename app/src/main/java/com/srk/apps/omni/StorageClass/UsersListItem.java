package com.srk.apps.omni.StorageClass;

import java.io.Serializable;

/**
 * OMNI
 * by SRK on 01-02-2016 @ 23:50
 *
 * @SYNAPPS 2016
 */

public class UsersListItem implements Serializable {
    public Integer userID;
    public String username;
    public String description;
    public Integer isFriend;
    public String profile_pic;
    public Integer hidden_Avatar;
}
