package com.srk.apps.omni.Interfaces;

import com.srk.apps.omni.SyncService.FriendsRequest;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * OMNI
 * by SRK on 13-03-2016 @ 14:32
 *
 * @SYNAPPS 2016
 */
public interface FriendRequestAPI {
    @GET("FriendsManager.php?action=action&myID=myID&toID=toID")
    Call<FriendsRequest> getResult(
            @Query("action") String action,
            @Query("myID") Integer myID,
            @Query("toID") Integer toID);
}
