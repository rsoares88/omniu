package com.srk.apps.omni.StorageClass;

import java.io.Serializable;

/**
 * OMNI
 * by SRK on 07-02-2016 @ 02:08
 *
 * @SYNAPPS 2016
 */
public class ChatMessage implements Serializable {

    public int fromUser;
    public String uniqueID;
    public int readTime = 0;
    public String TimeStamp = "null";


    public boolean left;
    public String message;

    public ChatMessage(boolean ob_left, int ob_fromUser, String ob_message, String ob_uniqueID) {
        super();
        this.left = ob_left;
        this.fromUser = ob_fromUser;
        this.message = ob_message;
        this.uniqueID = ob_uniqueID;
    }

    public String getMessage() {
        //TODO DECRYPT MESSAGE
        return message;
    }
}
