package com.srk.apps.omni.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.srk.apps.omni.Activitys.AppView;
import com.srk.apps.omni.R;

/**
 * OMNI
 * by SRK on 15-02-2016 @ 14:10
 *
 * @SYNAPPS 2016
 */
public class MainOptionsDialog extends DialogFragment {

    @Override public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.dimAmount = 0.70f;
        windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setAttributes(windowParams);
    }

    View view_instance;
    ImageView op_messages;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AppView.instance);
        // Get the layout inflater
        LayoutInflater inflater = AppView.instance.getLayoutInflater();
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        view_instance = inflater.inflate(R.layout.options_dialog, null);
        builder.setView(view_instance);

        op_messages = (ImageView) view_instance.findViewById(R.id.op_messages);
        op_messages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                Toast.makeText(getActivity().getApplicationContext(), "OnClick",
                        Toast.LENGTH_SHORT).show();
                cancelDialog();
                AppView.instance.OpenMessagesListFragment();
                //Open Messages List

            }
        });

        return builder.create();
    }

    void cancelDialog() {
        this.getDialog().cancel();
    }

}
