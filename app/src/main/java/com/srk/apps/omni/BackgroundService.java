package com.srk.apps.omni;

import android.Manifest;
import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.srk.apps.omni.Activitys.MainActivity;
import com.srk.apps.omni.Interfaces.UpdateLocationAPI;
import com.srk.apps.omni.Krypton.Kryptonite;
import com.srk.apps.omni.StorageClass.StorePrefs;
import com.srk.apps.omni.StorageClass.User;
import com.srk.apps.omni.SyncService.HasService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * OMNI
 * by SRK on 26-01-2016 @ 20:34
 *
 * @SYNAPPS 2016
 */
public class BackgroundService extends IntentService implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    // Must create a default constructor
    public BackgroundService() {
        // Used to name the worker thread, important only for debugging.
        super("BackgroundService");
    }

    public void LogService() {
        Log.e("TAG", "I CAN TALK TO BACKGROUND SERVICE");
    }

    User userData = null;

    public static BackgroundService instance = null;

    @Override
    public void onCreate() {
        super.onCreate(); // if you override onCreate(), make sure to call super().
        // If a Context object is needed, call getApplicationContext() here.
        instance = this;
        Log.d("TAG", "Reached onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handler.postDelayed(runnable, 1000);
        API_BASE_URL = getResources().getString(R.string.API_BASE_URL);
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d("TAG", "Reached onBind");
        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // This describes what will happen when service is triggered
        Log.d("TAG", "Reached onHandleIntent");
    }


    Handler handler = new Handler();

    Runnable runnable = new Runnable() {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            StartService();
            //handler.postDelayed(this, 1000); // 1000 - Milliseconds
        }
    };

    private void StartService() {
        Log.e("BGS", "SERVICE IS RUNNING");
        userData = new StorePrefs().LoadUserPrefs(instance); //.ResetSharedPrefs(BackgroundService.instance);
        if (userData.UserID != 0) {
            StartLocationServices();
        } else {
            Log.e("API", "FAILED TO GET USER DATA FOR BG SERVICE");
        }

    }

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private Location mLastLocation;
    private LocationRequest mLocationRequest;

    public static Location getLocation() {
        return instance.mLastLocation;
    }

    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;

    // boolean flag to toggle periodic location updates
    private boolean mRequestingLocationUpdates = true;

    // Location updates intervals in sec
    private static int UPDATE_INTERVAL = 60000;  // 60000; 1min //  10000; 10 sec // 60 * 60 * 1000; 1hora
    private static int FATEST_INTERVAL = 5000; // 5 sec 5000
    private static float DISPLACEMENT = 0.1f; // 10 meters

    //BETTER LOCATION
    private int MAX_UPDATE_TIME = 120000;

    void StartLocationServices() {
        if (checkPlayServices()) {

            // Building the GoogleApi client
            buildGoogleApiClient();
            createLocationRequest();
            mGoogleApiClient.connect();
        }
        // Resuming the periodic location updates

    }

    private void displayLocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi
                .getLastLocation(mGoogleApiClient);

        if (mLastLocation != null) {
            double latitude = mLastLocation.getLatitude();
            double longitude = mLastLocation.getLongitude();
            //lblLocation.setText(latitude + ", " + longitude);
            Log.e("LOC-UPD", "LAT: " + latitude + " LON: " + longitude);
            SendLocationUpdates();
        } else {
            Log.e("LOC-UPD", "(Couldn't get the location. Make sure location is enabled on the device)");
            //();
        }
    }


    /**
     * Creating google api client object
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    /**
     * Creating location request object
     */
    protected void createLocationRequest() {
        Log.i("API", "CREATED LOCATION REQUEST");
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);

        //
    }

    /**
     * Method to verify google play services on the device
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(this);
        if (result != ConnectionResult.SUCCESS) {
            return false;
        }
        return true;
    }

    /**
     * Starting the location updates
     */
    boolean LOCATION_REQUESTS_STATUS = false;

    protected void startLocationUpdates() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
        LOCATION_REQUESTS_STATUS = true;


    }

    /**
     * Stopping location updates
     */
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }

    /**
     * Google api callback methods
     */
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i("TAG", "Connection failed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }

    @Override
    public void onConnected(Bundle arg0) {

        // Once connected with google api, get the location
        //displayLocation();

        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }

    int UpdateCount = 0;
    int ValidUpdates = 0;
    @Override
    public void onLocationChanged(Location location) {
        //check if last time update is to old
        // Assign the new location
        UpdateCount++;
        Activity mainthread = MainActivity.mainactivity;
        Activity mapsthread = MapsActivity.instance;
        TextView updatesCount;


        if(isBetterLocation(location, mLastLocation)) {
            mLastLocation = location;
            // Proccess the new location
            displayLocation();
            ValidUpdates++;
            if (mainthread != null) {
                //MainActivity.toastMsg("LOCATION SERVICES: " + ServiceAvailable, 1);
                //location.distanceBetween(38.7619881, -9.1152819, latt, lonn, results);
                Log.e("API", "HAS MAIN THREAD");
                //Toast.makeText(mainthread, "LOCATION UPDATE ACCURACY: " + location.getAccuracy(), Toast.LENGTH_SHORT).show();
            }
        }
        //got new location log msg
        Log.e("API", "GOT NEW LOCATION UPDATE");
        if (mainthread != null && mapsthread != null) {
            //MainActivity.toastMsg("LOCATION SERVICES: " + ServiceAvailable, 1);
            //location.distanceBetween(38.7619881, -9.1152819, latt, lonn, results);
            //updatesCount

            updatesCount = (TextView) mapsthread.findViewById(R.id.updatesCount);



            Log.e("API", "HAS MAIN THREAD");
            Toast.makeText(mainthread, "ACCURACY: " + location.getAccuracy() + " BTL: " + isBetterLocation(location, mLastLocation), Toast.LENGTH_SHORT).show();
            updatesCount.setText("Updates: " + String.valueOf(UpdateCount) + "BTL: " + String.valueOf(ValidUpdates)
            + " CAC: " + location.getAccuracy()
            + " FI: " + mLocationRequest.getFastestInterval()
            + " FR: " + mLocationRequest.getInterval()
            + " FD: " + mLocationRequest.getSmallestDisplacement()
            + " UC: " + mLocationRequest.getNumUpdates()
            );
        }
    }


    private String API_BASE_URL;

    void SendLocationUpdates() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Kryptonite mcrypt = new Kryptonite();
            /* Encrypt */
        String encrypted = null;
        String latitude = String.valueOf(mLastLocation.getLatitude());
        String longitude = String.valueOf(mLastLocation.getLongitude());
        Integer userID = userData.UserID;
        try {
            encrypted = Kryptonite.bytesToHex(mcrypt.encrypt("update_location"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        String cryted_Action = encrypted;
        Log.e("KRYPTON", "UPDATE_LOCATION (KRYPTED) : " + cryted_Action);
        // Create an instance of our API interface.
        UpdateLocationAPI service = retrofit.create(UpdateLocationAPI.class);
        Log.e("KRYPTON", "UPDATE_LOCATION FOR: " + userID + " LAT: " + latitude + " LON: " + longitude);
        Call<HasService> call = service.getResult(cryted_Action, latitude, longitude, userID);
        //Log.e("KRYPTON", "sessionID: " + old_user.SessionID + " VALIDATE SESSION ID" + sessionID + " DEVICEID: " + deviceID);
        call.enqueue(new Callback<HasService>() {
            @Override
            public void onResponse(Response<HasService> response) {
                Log.e("Response2 code ", String.valueOf(response.code()));
                HasService result = response.body();
                String update_result = result.getResult();
                Log.e("KRYPTON-RESULT", "LOCATION UPDATE (DECODED): " + update_result);
                if (update_result.equals("true")) {
                    //LOCATION UPDATE SUCCESS
                    Log.d("API", "USER LOCATION @ SERVER UPDATED: " + update_result);
                } else {
                    //LOCATION UPDATE FAILED
                    Log.d("API", "USER LOCATION @ SERVER FAILED " + update_result);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("Response3 code ", String.valueOf(t));
                System.out.println(t.getMessage());
                System.out.println(t.getCause());
                //LOCATION UPDATE FAILED
                Log.d("API", "USER LOCATION @ SERVER FAILED");
                //Something went wrong, show login screen
                //reset stored prefs

            }
        });
    }

    public Boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            Log.d("API", "NO PREVIEW LOCATION, FORCE UPDATE");
            return true;
        }

        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyOlder = timeDelta > MAX_UPDATE_TIME;
        boolean isNewer = timeDelta > 0;

        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta >= 0;
        boolean isMoreAccurate = accuracyDelta < 0;

        //last location is to old, force update
        if (isSignificantlyOlder) {
            Log.d("API", "LAST UPDATE WAS TO OLD, FORCE UPDATE");
            return true;
        } else {
            //its a updated location, check if its more acurate
            if (isNewer && !isLessAccurate) {
                //its more accurate location
                Log.d("API", "IS NEWER AND MORE ACCURATE");
                return true;
            } else {
                if (isMoreAccurate) {
                    Log.d("API", "MIGTH BE OLDER BUT WAS MORE ACCURATE");
                    return true;
                }
                Log.d("API", "COULDN'T DEFINE LOCATION ACCURACY");
                return false;
            }
        }
    }

}
