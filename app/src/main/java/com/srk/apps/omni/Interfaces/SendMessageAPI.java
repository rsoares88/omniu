package com.srk.apps.omni.Interfaces;

import com.srk.apps.omni.SyncService.SentMessage;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * OMNI
 * by SRK on 07-02-2016 @ 00:54
 *
 * @SYNAPPS 2016
 */
public interface SendMessageAPI {
    @GET("send_message.php?action=action&ToUserID=ToUserID&FromUserID=FromUserID&message=message&uID=uID")
    Call<SentMessage> sendMessage(
            @Query("action") String action,
            @Query("ToUserID") Integer ToUserID,
            @Query("FromUserID") Integer FromUserID,
            @Query("message") String message,
            @Query("uID") String uID);
}
