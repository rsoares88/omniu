package com.srk.apps.omni.GCM;

import android.util.Log;

import com.google.android.gms.iid.InstanceID;
import com.google.android.gms.iid.InstanceIDListenerService;
import com.srk.apps.omni.R;
import com.srk.apps.omni.StorageClass.StorePrefs;
import com.srk.apps.omni.StorageClass.User;

import java.io.IOException;

/**
 * OMNI
 * by SRK on 04-02-2016 @ 00:41
 *
 * @SYNAPPS 2016
 */
public class MyInstanceIDService extends InstanceIDListenerService {
    public void onTokenRefresh() {
        refreshAllTokens();
    }

    private void refreshAllTokens() {
        String GCM_SENDER_ID = getResources().getString(R.string.GCM_SENDER_ID);//"365122193491";
        // assuming you have defined TokenList as
        // some generalized store for your tokens
        InstanceID iid = InstanceID.getInstance(this);
        Log.e("REFRESH-TOKEN", "GENERATE NEW TOKEN");
        try {

            //delete tokenID
            iid.deleteToken(GCM_SENDER_ID,"GCM");
            //delete instanceID
            iid.deleteInstanceID();

            InstanceID new_iid = InstanceID.getInstance(this);
            String token = new_iid.getToken(GCM_SENDER_ID,"GCM");

            StorePrefs storage = new StorePrefs();
            int CurrVersion = storage.getAppVersion(this);
            storage.SaveGCMInstanceID(token, CurrVersion, this);

            //TODO UPDATE TOKEN FOR THIS USER
            User userdata = storage.LoadUserPrefs(this);
            if(userdata.UserID !=0 || userdata.UserID != null) {
                //found registered user, send new token to server
            }

        } catch (IOException e) {
            e.printStackTrace();
            Log.e("REFRESH-TOKEN", "FAILED TO GENERATE NEW TOKEN");
        }
        // send this tokenItem.token to your server

    }
};