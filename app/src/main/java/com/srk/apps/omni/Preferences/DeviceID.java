package com.srk.apps.omni.Preferences;

import android.content.Context;
import android.provider.Settings;

/**
 * OMNI
 * by SRK on 26-01-2016 @ 11:53
 *
 * @SYNAPPS 2016
 */
public class DeviceID {

    public String getDeviceID(Context ctx) {

        String android_id = Settings.Secure.getString(ctx.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return android_id;
    }

}
