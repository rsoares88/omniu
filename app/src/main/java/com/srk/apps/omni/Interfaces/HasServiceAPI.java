package com.srk.apps.omni.Interfaces;

import com.srk.apps.omni.SyncService.HasService;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * OMNI
 * by SRK on 22-01-2016 @ 23:34
 *
 * @SYNAPPS 2016
 */
public interface HasServiceAPI {
    @GET("router.php?action=action&debug=debug")
    Call<HasService> getServers(
            @Query("action") String action,
            @Query("debug") Boolean debug);
}
