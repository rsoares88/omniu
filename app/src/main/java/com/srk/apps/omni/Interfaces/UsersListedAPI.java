package com.srk.apps.omni.Interfaces;

import com.srk.apps.omni.SyncService.UsersListed;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * OMNI
 * by SRK on 02-02-2016 @ 16:50
 *
 * @SYNAPPS 2016
 */
//action getUsersDetails
public interface UsersListedAPI {
    @GET("LocationService.php?action=action&userID=userID&rawList=rawList")
    Call<UsersListed> getDetails(
            @Query("action") String action,
            @Query("userID") Integer userID,
            @Query("rawList") String rawList);
}
