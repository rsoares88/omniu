package com.srk.apps.omni.SyncService;

/**
 * OMNI
 * by SRK on 01-02-2016 @ 14:56
 *
 * @SYNAPPS 2016
 */
public class ScanedResults {

    public String result;
    public Integer usersCount;
    public String rawList;


    public int[] getUsersList() {
        String arr = rawList;
        String[] items = arr.split(",");
        int[] results = new int[items.length];
        for (int i = 0; i < items.length; i++) {
            try {
                results[i] = Integer.parseInt(items[i]);
            } catch (NumberFormatException nfe) {}
        }
        return results;
    }
    //add other scan results

}
