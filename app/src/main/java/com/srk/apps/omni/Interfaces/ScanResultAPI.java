package com.srk.apps.omni.Interfaces;

import com.srk.apps.omni.SyncService.ScanedResults;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * OMNI
 * by SRK on 01-02-2016 @ 14:54
 *
 * @SYNAPPS 2016
 */
public interface ScanResultAPI {
    @GET("LocationService.php?action=action&userID=userID")
    Call<ScanedResults> getScan(
            @Query("action") String action,
            @Query("userID") Integer userID);
}
