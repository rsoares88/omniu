package com.srk.apps.omni.Activitys;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.res.Configuration;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.location.LocationServices;
import com.srk.apps.omni.BackgroundService;
import com.srk.apps.omni.GCM.GCMCreateInstanceID;
import com.srk.apps.omni.Helpers.ConnectionDetector;
import com.srk.apps.omni.Interfaces.HasAccountAPI;
import com.srk.apps.omni.Interfaces.HasServiceAPI;
import com.srk.apps.omni.Interfaces.LoginUserAPI;
import com.srk.apps.omni.Interfaces.ValidSessionAPI;
import com.srk.apps.omni.Krypton.Kryptonite;
import com.srk.apps.omni.Preferences.DeviceID;
import com.srk.apps.omni.R;
import com.srk.apps.omni.StorageClass.StorePrefs;
import com.srk.apps.omni.StorageClass.User;
import com.srk.apps.omni.SyncService.HasAccount;
import com.srk.apps.omni.SyncService.HasService;
import com.srk.apps.omni.SyncService.LoginUser;
import com.srk.apps.omni.SyncService.ValidSession;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    //TODO (EASY LOGIN) CHANGE DEBUG VAR TO FALSE (true)
    protected Boolean isDebug = true;

    // Google API
    public static final int REQUEST_CODE_RESOLUTION = 1972;
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static MainActivity mainactivity;
    public static boolean PLAY_SERVICES_CONNECTED = false;

    public static String GCM_SENDER_ID;

    private static GoogleApiClient mGoogleApiClient;
    public String API_BASE_URL;// = getApplicationContext().getString(R.string.API_BASE_URL); //"@string/API_BASE_URL";
    private CallbackManager callbackManager;
    private LoginButton loginButton;
    private Button fb_login;
    private LinearLayout signin_layout;
    private FrameLayout errorFrame;
    private TextView topic_txt;
    private TextView result_txt;
    private User user;

    //TODO [CODE] SPLASH SCREEN!!!!!!!!!!!!!!!
    private Boolean USER_BACK_PRESSED = false;
    private final Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            USER_BACK_PRESSED = false;
        }
    };
    private Toast exit_toast;
    private Handler mHandler = new Handler();
    private EditText username;
    private EditText password;
    private FacebookCallback<LoginResult> mCallBack = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            //progressDialog.dismiss();

            // App code
            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {

                            Log.e("response: ", response + "");
                            Log.e("object: ", object + "");
                            try {
                                user = new User();

                                user.FirstName = (object.getString("first_name"));
                                user.LastName = (object.getString("last_name"));
                                if (object.getString("gender").equals("male")) {
                                    user.Gender = 0;
                                } else {
                                    user.Gender = 1;
                                }

                                user.Email = (object.getString("email"));

                                user.isFacebook = true;
                                user.FacebookID = (object.getString("id"));
                                user.BirthDate = (object.getString("birthday"));

                                //avatar
                                JSONObject avatar_source = new JSONObject(object.getString("picture"));
                                JSONObject avatar_source_1 = new JSONObject(avatar_source.getString("data"));
                                String avatar_source_uri = (String) avatar_source_1.get("url");
                                user.Avatar_Img_Link = avatar_source_uri;

                                JSONObject cover_source = new JSONObject(object.getString("cover"));
                                String cover_source_uri = (String) cover_source.get("source");
                                user.Cover_Img_Link = cover_source_uri;
                                //PrefUtils.setCurrentUser(user,MainActivity.this);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            //TODO now check if the user already is registered in database
                            Retrofit retrofit = new Retrofit.Builder()
                                    .baseUrl(API_BASE_URL)
                                    .addConverterFactory(GsonConverterFactory.create())
                                    .build();

                            Kryptonite mcrypt = new Kryptonite();
                             /* Encrypt */
                            String encrypted = null;
                            try {
                                encrypted = Kryptonite.bytesToHex(mcrypt.encrypt("has_fb_account"));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            String cryted_Action = encrypted;
                            Log.e("KRYPTON", "ENCODED VALUE: " + cryted_Action);
                            // Create an instance of our API interface.
                            HasAccountAPI service = retrofit.create(HasAccountAPI.class);
                            Call<HasAccount> call = service.getResult(cryted_Action, user.FacebookID);

                            call.enqueue(new Callback<HasAccount>() {
                                @Override
                                public void onResponse(Response<HasAccount> response) {
                                    Log.e("Response2 code ", String.valueOf(response.code()));
                                    if (String.valueOf(response.code()).equals("500")) {
                                        Toast.makeText(MainActivity.mainactivity, "Signin Failed : " + String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                    HasAccount result = response.body();
                                    String value = result.getResult();

                                    if (value == null) {
                                        Log.e("FB-RESULT", "NO ACTION SET:EXIT " + value);
                                        DisplayErrorFrame("ERROR 505", "Something went wrong!");
                                        return;
                                    }

                                    switch (value) {
                                        case "true": {
                                            //already registered, TODO login the user with facebook account
                                            //DO FAKE LOGIN
                                            Log.e("FB-RESULT", "already registered, login the user with facebook account" + value);
                                            user.isFacebook = true;
                                            LoginUser();
                                            break;
                                        }
                                        case "false": {
                                            //the is a new account, OPEN PROFILE CREATION
                                            Log.e("FB-RESULT", "not registered, the is a new account, OPEN PROFILE CREATION" + value);
                                            openProfileCreation(true, user);
                                            break;
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Throwable t) {
                                    Log.e("Response3 code ", String.valueOf(t));
                                    System.out.println(t.getMessage());
                                    System.out.println(t.getCause());
                                }
                            });
                            finish();
                        }

                    });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "id, first_name, last_name, email, gender, birthday, cover, picture.type(large)");
            request.setParameters(parameters);
            request.executeAsync();
        }

        @Override
        public void onCancel() {
            //progressDialog.dismiss();
        }

        @Override
        public void onError(FacebookException e) {
            // progressDialog.dismiss();
        }
    };

    private boolean checkPlayServices() {
        if (!isNetworkConnected()) {
            return false;
        }
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(this);
        if (result != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(this, result,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }
            return false;
        }
        return true;
    }

    boolean isNetworkConnected() {
        return ConnectionDetector.isNetworkConnected(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        mainactivity = this;
        setContentView(R.layout.activity_main);
        DisplayErrorFrame("CONNECTING", "Please wait!");
        API_BASE_URL = getResources().getString(R.string.API_BASE_URL);

        //initialize buttons effects // simulate clicks
        Button login_btn = (Button) findViewById(R.id.login_btn);
        Button createAccount = (Button) findViewById(R.id.createAccount);
        buttonEffect(login_btn);
        buttonEffect(createAccount);
        //TODO TEST CODE


        //TODO ENDED CODE TEST
        if (savedInstanceState == null) {
            //check if client has internet
            if (isNetworkConnected()) {
                handler.postDelayed(runnable, interval);
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(API_BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                Kryptonite mcrypt = new Kryptonite();
                /* Encrypt */
                String encrypted = null;
                try {
                    encrypted = Kryptonite.bytesToHex(mcrypt.encrypt("has_service"));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                String cryted_Action = encrypted;
                Log.e("KRYPTON", "ENCODED VALUE: " + cryted_Action);
                // Create an instance of our API interface.
                HasServiceAPI service = retrofit.create(HasServiceAPI.class);
                Call<HasService> call = service.getServers(cryted_Action, false);
                mainactivity = this;
                call.enqueue(new Callback<HasService>() {

                    @Override
                    public void onResponse(Response<HasService> response) {
                        String responseCode = String.valueOf(response.code());
                        Log.e("Response2 code ", responseCode);
                        if(responseCode.equals("404")) {
                            Log.e("ERROR: 404", "CODE VALUE: " + responseCode);
                            no_service("ERROR: 404", "Invalid response, check your connection or try again later.");
                            return;
                        }

                        //Log.e("Response Message", response.message() + " result= " + response.body().result + " headers: " + response.headers());
                        String value = response.body().getResult();
                        Log.e("KRYPTON-RESULT", "DECODED VALUE: " + value);
                        if (value.equals("online")) {
                            //MAIN SERVERS ARE ONLINE PROCEED
                            Log.i("TAG", "has_service: " + value);
                            has_Service();
                        }
                        if (value.equals("maintenance")) {
                            //MAIN SERVER IS IN MAINTENANCE
                            no_service("Maintenance", "The servers are offline, we promise to be short!");
                            //no_service
                        }
                        if (!value.equals("maintenance") && !value.equals("online")) {
                            no_service("ERROR: 10", "Invalid response, try again later.");
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        Log.e("Response3 code ", String.valueOf(t));
                        no_service("Maintenance", "Cant connect to main server!");
                        System.out.println(t.getMessage());
                        System.out.println(t.getCause());
                    }
                });
            } else {
                //no internet connection
                DisplayErrorFrame("No internet connection", "Please turn on internet service");
            }
        }
        handler.postDelayed(runnable, interval);
    }

    int maxTimeout = 20;
    private int CONNECTION_FRAME_TIMEOUT = maxTimeout;
    private int interval = 1000;
    private boolean isValidated = false;
    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        public void run() {
            if (CONNECTION_FRAME_TIMEOUT > 0 && !isValidated) {
                CONNECTION_FRAME_TIMEOUT--;
                handler.postDelayed(runnable, interval);
            } else if (CONNECTION_FRAME_TIMEOUT <= 0) {
                //operation time out frame
                no_service("TIMEOUT", "Operation timeout.");
                CONNECTION_FRAME_TIMEOUT = maxTimeout;
                isValidated = false;
            }
        }
    };

    public void has_Service() {
        //SERVER IS ONLINE CHECK IF PLAYSERVICE IS AVAILABLE
        if (checkPlayServices()) {
            isValidated = true;
            Log.d("has-service", "SERVER ONLINE - AND PLAYSERVICE OK");
            signin_layout = (LinearLayout) this.findViewById(R.id.signin_layout);
            buildGoogleApiClient();

            //USER HAS INTERNETS, CREATE GMC INSTANCE ID
            GCM_SENDER_ID = this.getResources().getString(R.string.GCM_SENDER_ID);


            String instanceID = getInstanceID();
            Boolean VersionMatch = gcmVersionMatch();

            if (instanceID == null) {
                GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                new GCMCreateInstanceID(this, gcm).execute();
            } else {
                Log.d("GCM-ID", instanceID);
                //User already has instanceID, check if its updated!
                if (!VersionMatch) {
                    //current token doesnt match current version
                    GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                    new GCMCreateInstanceID(this, gcm).execute();
                }
            }
        } else {
            //PLAYSERVICE IS NOT AVAILABLE
            DisplayErrorFrame("Invalid PlayService", "Please update your PlayStore / PlayService.");
        }
    }

    public String getInstanceID() {
        StorePrefs storage = new StorePrefs();
        String instanceID = storage.GetGCMInstanceID(this);
        return instanceID;
    }

    public boolean gcmVersionMatch() {
        StorePrefs storage = new StorePrefs();
        Boolean VersionMatch = storage.MatchVersions(this);
        return VersionMatch;
    }

    public void DisplayErrorFrame(String topic, String value) {
        if (errorFrame == null) {
            errorFrame = (FrameLayout) mainactivity.findViewById(R.id.errorFrame);
            topic_txt = (TextView) mainactivity.findViewById(R.id.topic_txt);
            result_txt = (TextView) mainactivity.findViewById(R.id.result_txt);
        }
        errorFrame.setVisibility(View.VISIBLE);
        topic_txt.setText(topic);
        result_txt.setText(value);
    }

    public void no_service(String topic, String resultMsg) {
        DisplayErrorFrame(topic, resultMsg);
        Button btn_retry = (Button) findViewById(R.id.btn_retry);
        btn_retry.setVisibility(View.VISIBLE);
    }

    public void btn_retry(View view) {

        Button btn_retry = (Button) findViewById(R.id.btn_retry);
        btn_retry.setVisibility(View.GONE);

        Intent i = getApplicationContext().getPackageManager()
                .getLaunchIntentForPackage(getApplicationContext().getPackageName());

        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

    @Override
    public void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);

        if (!checkPlayServices()) {
            DisplayErrorFrame("Invalid PlayService", "Please update your PlayStore / PlayService.");
            //finish();
        }
        if (PLAY_SERVICES_CONNECTED && checkPlayServices()) {
            InitializeReady();
        }

        callbackManager = CallbackManager.Factory.create();
        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("public_profile", "email", "user_friends", "user_birthday");

        fb_login = (Button) findViewById(R.id.fb_login);
        fb_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainactivity.buttonEffect(v);
                loginButton.performClick();
                loginButton.setPressed(true);
                loginButton.invalidate();
                loginButton.registerCallback(callbackManager, mCallBack);
                loginButton.setPressed(false);
                loginButton.invalidate();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (USER_BACK_PRESSED) {
            if (this.exit_toast != null)
                this.exit_toast.cancel();
            super.onBackPressed();
            return;
        }
        // User at main screen, double tap to exit
        this.exit_toast = Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT);
        this.USER_BACK_PRESSED = true;
        this.exit_toast.show();
        mHandler.postDelayed(mRunnable, 2000);
    }

    @Override
    public void onDestroy() {
        this.mainactivity = null;

        if (mHandler != null) {
            mHandler.removeCallbacks(mRunnable);
        }
        if (exit_toast != null)
            exit_toast.cancel();
        super.onDestroy();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void openProfileCreation(Boolean isFacebook, User userdata) {
        //o
        if (isFacebook) {
            Log.e("DATA", userdata.toString());
            //create facebook intent
            Intent intent = new Intent(MainActivity.this, Create_Profile.class);
            intent.putExtra("userdata", userdata);
            intent.putExtra("isFacebook", isFacebook);
            startActivity(intent);
        } else {
            //create email intent
            Intent intent = new Intent(MainActivity.this, Create_Profile.class);
            intent.putExtra("userdata", userdata);
            intent.putExtra("isFacebook", isFacebook);
            startActivity(intent);
            this.finish();
        }
        signin_layout = (LinearLayout) mainactivity.findViewById(R.id.signin_layout);
        signin_layout.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d("LCD", "GOOGLE API CONNECTED");
        //ok now you can start the App
        PLAY_SERVICES_CONNECTED = true;
        //NOW FINALY SHOW THE LOGIN BUTTONS
        InitializeReady();
    }

    private void ShowLoginScreen() {
        if (signin_layout == null) {
            signin_layout = (LinearLayout) mainactivity.findViewById(R.id.signin_layout);
        }
        if (errorFrame.isShown()) {
            errorFrame.setVisibility(View.GONE);
        }
        if (!signin_layout.isShown()) {
            signin_layout.setVisibility(View.VISIBLE);
        }
    }

    public void InitializeReady() {
        //check if the user is already logged
        StorePrefs prefs_storage = new StorePrefs();
        final User old_user = null;

        try {
            prefs_storage.LoadUserPrefs(this);
        } catch (Exception e) {
            // its not an old user
        }
        //USER DATA NOT FOUND, ITS A FIRST TIME LOGIN
        if (old_user == null) {
            ShowLoginScreen();
        } else {
            //ITS A RETURNING USER, VALIDATE OLD SESSION
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            Kryptonite mcrypt = new Kryptonite();
            /* Encrypt */
            String encrypted = null;
            String sessionID = null;
            String deviceID = null;
            try {
                encrypted = Kryptonite.bytesToHex(mcrypt.encrypt("validate_session"));
                sessionID = Kryptonite.bytesToHex(mcrypt.encrypt(old_user.SessionID));
                deviceID = Kryptonite.bytesToHex(mcrypt.encrypt(new DeviceID().getDeviceID(this)));
            } catch (Exception e) {
                e.printStackTrace();
            }

            String cryted_Action = encrypted;
            Log.e("KRYPTON", "VALIDATE SESSION ACTION: " + cryted_Action);
            // Create an instance of our API interface.
            ValidSessionAPI service = retrofit.create(ValidSessionAPI.class);
            Call<ValidSession> call = service.getResult(cryted_Action, sessionID, deviceID);
            Log.e("KRYPTON", "sessionID: " + old_user.SessionID + " VALIDATE SESSION ID" + sessionID + " DEVICEID: " + deviceID);
            call.enqueue(new Callback<ValidSession>() {
                @Override
                public void onResponse(Response<ValidSession> response) {
                    Log.e("Response2 code ", String.valueOf(response.code()));
                    ValidSession result = response.body();
                    Boolean valid_session = result.getResult();
                    Log.e("KRYPTON-RESULT", "VALID SESSION (DECODED): " + valid_session);
                    if (valid_session) {
                        //valid session, Authorize user
                        Log.i("TAG", "valid_session: " + valid_session + " PASSCODE: " + old_user.Password);
                        //TODO RELOGIN USER, (ALWAYS NEEDS TO VALIDATE DATA)
                        //
                        StorePrefs prefs_storage = new StorePrefs();
                        User old_user = prefs_storage.LoadUserPrefs(mainactivity);
                        OpenAppView(old_user);
                    } else {
                        //invalid session
                        //reset stored prefs
                        new StorePrefs().ResetSharedPrefs(mainactivity);
                        //show login screen
                        ShowLoginScreen();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    Log.e("Response3 code ", String.valueOf(t));
                    System.out.println(t.getMessage());
                    System.out.println(t.getCause());
                    //Something went wrong, show login screen
                    //reset stored prefs
                    new StorePrefs().ResetSharedPrefs(mainactivity);
                    //show login screen
                    ShowLoginScreen();
                }
            });
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i("GAPI", "GoogleApiClient connection SUSPENDED: " + i);
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Called whenever the API client fails to connect.
        Log.i("GAPI", "GoogleApiClient connection failed: " + result.toString());
        if (!result.hasResolution()) {
            // show the localized error dialog.
            GoogleApiAvailability.getInstance().getErrorDialog(this, result.getErrorCode(), 0).show();
            return;
        }
        // The failure has a resolution. Resolve it.
        // Called typically when the app is not yet authorized, and an
        // authorization
        // dialog is displayed to the user.
        try {
            result.startResolutionForResult(this, REQUEST_CODE_RESOLUTION);
        } catch (IntentSender.SendIntentException e) {
            Log.e("GAPI", "Exception while starting resolution activity", e);
        }
    }

    public void emailLoginButton(View view) {
        buttonEffect(view);
        openProfileCreation(false, user);
    }

    public void buttonEffect(View button) {
        button.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        v.getBackground().setColorFilter(0xe0f47521, PorterDuff.Mode.SRC_ATOP);
                        v.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        v.getBackground().clearColorFilter();
                        v.invalidate();
                        break;
                    }
                }
                return false;
            }
        });
    }

    private Boolean isLoggin = false;

    public void doLogin(View view) {

        buttonEffect(view);

        if (isLoggin) {
            return;
        }

        user = new User();
        user.isFacebook = false;

        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);

        if (isDebug) {
            username.setText("zezinho");
            password.setText("20039292Az");
        }

        String text_username = username.getText().toString();
        String text_password = password.getText().toString();
        if (text_username.equals("Username") || text_password.equals("**********") || text_username == null || text_password == null) {
            Toast.makeText(this, "Please insert you username and password!", Toast.LENGTH_SHORT).show();
        } else {
            user.Username = text_username;
            user.Password = text_password;
            LoginUser();
        }

    }

    private void LoginUser() {
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);

        if (user.isFacebook) {
            user.Password = user.FacebookID;
            username.setText(user.Username);
            password.setText(user.Password);
        }
        //READY TO LOGIN, USER SPECIFIED DETAILS
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Kryptonite mcrypt = new Kryptonite();
                             /* Encrypt */
        String encrypted = null;
        String userN = Kryptonite.bytesToHex(mcrypt.encrypt(user.Username));
        String userP = Kryptonite.bytesToHex(mcrypt.encrypt(user.Password));
        String GcmInstanceID = getInstanceID();
        String deviceID = Kryptonite.bytesToHex(mcrypt.encrypt(new DeviceID().getDeviceID(this)));

        //Log.e("LOGIN", "USERNAME: " + userN + " PASSWORD: " + userP + " DEVICEID: " + new DeviceID().getDeviceID(this) + "INSTANCEID: " + GcmInstanceID);

        try {
            encrypted = Kryptonite.bytesToHex(mcrypt.encrypt("login_user"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        String cryted_Action = encrypted;
        //Log.e("KRYPTON", "ENCODED VALUE: " + cryted_Action);
        // Create an instance of our API interface.
        LoginUserAPI service = retrofit.create(LoginUserAPI.class);
        Call<LoginUser> call = service.getResult(cryted_Action, userN, userP, deviceID, GcmInstanceID);
        MainActivity.mainactivity = this;
        call.enqueue(new Callback<LoginUser>() {
            @Override
            public void onResponse(Response<LoginUser> response) {
                Log.e("Log Response code ", String.valueOf(response.body().getResult()));
                isLoggin = false;
                if (String.valueOf(response.code()).equals("500")) {
                    Toast.makeText(MainActivity.mainactivity, "Sign-in Failed : " + String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                    return;
                }

                LoginUser result = response.body();
                String value = result.getResult();

                if (value == null) {
                    //Log.e("FB-RESULT", "NO ACTION SET:EXIT " + value);
                    DisplayErrorFrame("ERROR 505", "Something went wrong!");
                    return;
                }

                switch (value) {
                    case "true": {
                        //Log.e("LOGIN", "LOGIN SUCCESS" + value);
                        Log.e("LOGIN", "LOGIN JSON USERDATA" + result.getUserData());
                        User FinalUser = result.updatedUserDetails(user);
                        Log.e("LOGIN", "LOAD HIDEN AVATAR: " + FinalUser.hiden_avatar);

                        //create store prefs
                        StorePrefs prefs_storage = new StorePrefs();
                        prefs_storage.StoreUserValues(FinalUser, MainActivity.mainactivity);
                        OpenAppView(FinalUser);
                        break;
                    }
                    case "false": {
                        //the is a new account, OPEN PROFILE CREATION
                        Log.e("LOGIN", "LOGIN FAILED" + value);
                        Toast.makeText(MainActivity.mainactivity, "Login Failed : " + result.getResult(), Toast.LENGTH_SHORT).show();
                        break;
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e(" LOGIN FAILURE ", String.valueOf(t));
                isLoggin = false;
                System.out.println(t.getMessage());
                System.out.println(t.getCause());
            }
        });
    }

    void OpenAppView(User FinalUser) {
        //Log.e("APP", "START NEW BACKGROUND SERVICE : " + isMyServiceRunning(BackgroundService.class));
        if (!isMyServiceRunning(BackgroundService.class)) {
            Intent service = new Intent(this, BackgroundService.class);
            service.putExtra("userdata", FinalUser);
            startService(service);
        } else {
            //do test function
            try {
                BackgroundService.instance.LogService();
            } catch (Exception e) {
                Intent intent = new Intent(MainActivity.this, AppView.class);
                intent.putExtra("userdata", FinalUser);
                intent.putExtra("flag", "main_view");
                startActivity(intent);
            }
        }

        Intent intent = new Intent(MainActivity.this, AppView.class);
        intent.putExtra("userdata", FinalUser);
        intent.putExtra("flag", "main_view");
        startActivity(intent);
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}
