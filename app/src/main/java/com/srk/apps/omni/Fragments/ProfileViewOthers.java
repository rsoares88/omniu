package com.srk.apps.omni.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.srk.apps.omni.Activitys.AppView;
import com.srk.apps.omni.Helpers.AvatarsList;
import com.srk.apps.omni.Interfaces.FriendRequestAPI;
import com.srk.apps.omni.R;
import com.srk.apps.omni.StorageClass.UsersListItem;
import com.srk.apps.omni.SyncService.FriendsRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileViewOthers extends Fragment {

    public static ProfileViewOthers instance;

    public ProfileViewOthers() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.instance = this;
    }

    TextView userName;
    TextView aboutU;
    ImageView profile_image;
    ImageView friendRequest;
    View view_instance;
    UsersListItem toUser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view_instance = inflater.inflate(R.layout.fragment_profile_view_others, container, false);
        Bundle args = getArguments();

        if (args != null && view_instance != null) {
            //build user profile
            toUser = (UsersListItem) args.getSerializable("toUser");
            if (toUser == null) {
                //TODO IF toUser is null print error / go back to selection
                return null;
            }

            userName = (TextView) view_instance.findViewById(R.id.userName);
            aboutU = (TextView) view_instance.findViewById(R.id.aboutU);
            profile_image = (ImageView) view_instance.findViewById(R.id.profile_image);
            userName.setText(toUser.username);
            aboutU.setText(toUser.description);

            friendRequest = (ImageView) view_instance.findViewById(R.id.friendRequest);
            Log.e("FRIEND", "VALUE: " + toUser.isFriend );
            if (toUser.isFriend == 0) {
                //TODO DISPLAY REMOVE LINK BUTTON
                friendRequest.setImageResource(R.drawable.friend_del);
                //is friend display public avatar
                String API_BASE_URL = view_instance.getContext().getString(R.string.AVATAR_IMG_BASE_URL);
                String Avatar_Img_Link = toUser.profile_pic;
                String final_path = API_BASE_URL + Avatar_Img_Link;

                Picasso.with(AppView.instance)
                        .load(final_path)
                        .placeholder(R.drawable.ic_placeholder) // optional
                        .error(R.drawable.ic_error_fallback)         // optional
                        .into(profile_image);
            } else {
                friendRequest.setImageResource(R.drawable.friend_add);
                profile_image.setImageResource(AvatarsList.ImageId[toUser.hidden_Avatar]);
            }


            if (toUser.isFriend != 0) {
                friendRequest.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.e("TAG", "Send friend request to user: " + toUser.username + " @: " + toUser.userID);
                        //fail safe code to prevent duplicate requests.
                        String API_BASE_URL = getResources().getString(R.string.API_BASE_URL);
                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl(API_BASE_URL)
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();

                        String cryted_Action = "Friend_Request";
                        // Create an instance of our API interface.
                        FriendRequestAPI service = retrofit.create(FriendRequestAPI.class);
                        Call<FriendsRequest> call = service.getResult(cryted_Action, AppView.instance.userdata.UserID, toUser.userID); //

                        call.enqueue(new Callback<FriendsRequest>() {
                            @Override
                            public void onResponse(Response<FriendsRequest> response) {
                                Log.e("Response code ", String.valueOf(response.code()));
                                String value = response.body().result;
                                Log.e("KRYPTON-RESULT", "DECODED VALUE: " + value);
                                if (value == null) {
                                    Log.e("RESULT", "GOT NULL RESPONSE");
                                    return;
                                }
                                switch (value) {
                                    case "true": {
                                        Log.i("SCAN-RESULT", "GOT SCAN RESULTS: " + response.body().result + " LIST: " + response.body());
                                        Toast.makeText(AppView.instance, "Link request sent to " + toUser.username, Toast.LENGTH_LONG).show();
                                        break;
                                    }
                                    case "fail": {
                                        Log.i("SCAN-RESULT", "FAILED SCAN: " + value);
                                        break;
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Throwable t) {
                                Log.e("FAILURE-SCANNING-RESULT", String.valueOf(t));
                                System.out.println(t.getMessage());
                                System.out.println(t.getCause());
                            }
                        });

                    }
                });
            } else {
                // remove friend
                friendRequest.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.e("TAG", "Remove friend with user: " + toUser.username + " @: " + toUser.userID);
                        //fail safe code to prevent duplicate requests.
                        String API_BASE_URL = getResources().getString(R.string.API_BASE_URL);
                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl(API_BASE_URL)
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();

                        String cryted_Action = "Friend_Remove";
                        // Create an instance of our API interface.
                        FriendRequestAPI service = retrofit.create(FriendRequestAPI.class);
                        Call<FriendsRequest> call = service.getResult(cryted_Action, AppView.instance.userdata.UserID, toUser.userID); //

                        call.enqueue(new Callback<FriendsRequest>() {
                            @Override
                            public void onResponse(Response<FriendsRequest> response) {
                                Log.e("Response code ", String.valueOf(response.code()));
                                String value = response.body().result;
                                Log.e("KRYPTON-RESULT", "DECODED VALUE: " + value);
                                if (value == null) {
                                    Log.e("RESULT", "GOT NULL RESPONSE");
                                    return;
                                }
                                switch (value) {
                                    case "true": {
                                        Log.i("SCAN-RESULT", "GOT SCAN RESULTS: " + response.body().result + " LIST: " + response.body());
                                        Toast.makeText(AppView.instance, "Link request sent to " + toUser.username, Toast.LENGTH_LONG).show();
                                        break;
                                    }
                                    case "fail": {
                                        Log.i("SCAN-RESULT", "FAILED SCAN: " + value);
                                        break;
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Throwable t) {
                                Log.e("FAILURE-SCANNING-RESULT", String.valueOf(t));
                                System.out.println(t.getMessage());
                                System.out.println(t.getCause());
                            }
                        });

                    }
                });
            }

        }
        return view_instance;
    }

}
