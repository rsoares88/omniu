package com.srk.apps.omni.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.srk.apps.omni.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmptyFragment extends Fragment {

    public static EmptyFragment instance;

    public EmptyFragment() {
        // Required empty public constructor
    }


    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
        Log.e("EMPTY-FRAG", "EMPTY FRAGMENT WAS CREATED");
    }


    @Override public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    View view_instance;
    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view_instance = inflater.inflate(R.layout.fragment_empty, container, false);

        Bundle args = getArguments();
        if (args != null && view_instance != null) {

            headerText = (TextView) view_instance.findViewById(R.id.headerText);
            progressScan = (ProgressBar) view_instance.findViewById(R.id.progressScan);

            String action = args.getString("action"); //args.getString("action", null);

            switch (action) {
                case "start_scan": {
                    String text =  getResources().getString(R.string.empty_fragment_scanning);
                    headerText.setText(text);
                    progressScan.setVisibility(View.VISIBLE);
                    break;
                }
                case "end_scan": {
                    headerText.setText( getResources().getString(R.string.empty_fragment_header));
                    progressScan.setVisibility(View.GONE);
                    break;
                }
                case "no_results": {
                    headerText.setText( getResources().getText(R.string.empty_fragment_NoResults));
                    progressScan.setVisibility(View.GONE);
                    break;
                }
                case "loading_chat": {
                    headerText.setText( getResources().getText(R.string.empty_fragment_default_loading));
                    progressScan.setVisibility(View.VISIBLE);
                }

                default: {
                    headerText.setText( getResources().getString(R.string.empty_fragment_header));
                    progressScan.setVisibility(View.GONE);
                }
            }
        }

        return view_instance;
    }



    TextView headerText;
    ProgressBar progressScan;
    void setViewElements() {
        if(view_instance !=  null) {
            headerText = (TextView) view_instance.findViewById(R.id.headerText);
            progressScan = (ProgressBar) view_instance.findViewById(R.id.progressScan);
        }
    }

    @Deprecated
    public void doStartedScan() {
        if(headerText == null || progressScan == null) {
            setViewElements();
        }
        String text =  "Scanning"; //getActivity().getResources().getString(R.string.empty_fragment_scanning);
            headerText.setText(text);
            progressScan.setVisibility(View.VISIBLE);
    }

    @Deprecated
    public void doEndScan() {
        if(headerText == null || progressScan == null) {
            setViewElements();
        }
        instance.headerText.setText( getResources().getString(R.string.empty_fragment_header));
        progressScan.setVisibility(View.GONE);
    }

    @Deprecated
    public void doNoResultsFound() {
        if(headerText == null || progressScan == null) {
            setViewElements();
        }
        instance.headerText.setText( getResources().getText(R.string.empty_fragment_NoResults));
        progressScan.setVisibility(View.GONE);
    }

}

