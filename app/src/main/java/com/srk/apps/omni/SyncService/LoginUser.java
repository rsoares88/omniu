package com.srk.apps.omni.SyncService;

import android.util.Log;

import com.srk.apps.omni.Krypton.Kryptonite;
import com.srk.apps.omni.StorageClass.User;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * OMNI
 * by SRK on 25-01-2016 @ 18:01
 *
 * @SYNAPPS 2016
 */
public class LoginUser {

    public String action;
    public String result;
    public String printMessage;
    public String sessionID;
    public String userData;

    public String TestString =
            "{\"sessionID\":\"166\",\"userID\":166,\"fb_id\":\"\\u00f2\\u00cc\\u00f8\\u000f\\u00fe@\\u00c9\\u008f\\u00c3P\\u00f8\\n`U9\\u0099\",\"email\":\"ricardo.mxp@gmail.com\",\"username\":\"serialkiler\",\"password\":\"$2y$11$ECN3vozPEHIiur08qHuxae5aonkqalKPKavcSI5Y3IpvgWzrd9A9e\",\"profile_ID\":51,\"firstname\":\"Ricardo\",\"lastname\":\"Ricardo\",\"gender\":0,\"avatar\":\"IMG-20160123-WA0000.jpeg\",\"cover\":\"IMG-20160123-WA0000.jpeg\",\"birthdate\":\"03-06-1988\",\"aboutu\":\"Sample text, delete debug helper function\"}";

    public String getResult() {
        try {
            Kryptonite mcrypt = new Kryptonite();
            String decrypted = mcrypt.decrypt(result);
            return decrypted;
        } catch (Exception e) {
            Log.e("KRYPTON", "FAILED TO DECODE RESULT = " + result);
            e.printStackTrace();
            return result;
        }
    }
    public String getSessionID() {
        try {
            Kryptonite mcrypt = new Kryptonite();
            String decrypted = mcrypt.decrypt(sessionID);
            return decrypted;
        } catch (Exception e) {
            Log.e("KRYPTON", "FAILED TO DECODE RESULT getSessionID = " + sessionID);
            e.printStackTrace();
            return sessionID;
        }
    }

    public String getUserData() {
        try {
            Kryptonite mcrypt = new Kryptonite();
            String decrypted = mcrypt.decrypt(userData);
            return decrypted;
        } catch (Exception e) {
            Log.e("KRYPTON", "FAILED TO DECODE RESULT getUserData = " + userData);
            e.printStackTrace();
            return userData;
        }
    }


    public User updatedUserDetails(User oldUser) {

        JSONObject resultObject = null;
        try {
            resultObject = new JSONObject(getUserData());
            oldUser.SessionID = resultObject.getString("sessionID");
            Log.v("HTTP-REPLY", "SESSION:  " + resultObject.getString("sessionID"));
            oldUser.UserID = Integer.parseInt(resultObject.get("userID").toString());
            if(oldUser.isFacebook) {
                oldUser.FacebookID = resultObject.getString("fb_id"); }
            oldUser.Email = resultObject.getString("email");

            oldUser.Username = resultObject.getString("username");
            oldUser.profile_ID = Integer.parseInt(resultObject.get("profile_ID").toString());

            oldUser.FirstName = resultObject.getString("firstname");
            oldUser.LastName =  resultObject.getString("lastname");
            oldUser.Gender = Integer.parseInt(resultObject.get("gender").toString());

            oldUser.hiden_avatar = Integer.parseInt(resultObject.get("hiden_avatar").toString());

            oldUser.Avatar_Img_Link = resultObject.getString("avatar");
            oldUser.Cover_Img_Link  = resultObject.getString("cover");
            oldUser.BirthDate  = resultObject.getString("birthdate");
            oldUser.AboutU = resultObject.getString("aboutu");
        return oldUser;
        } catch (JSONException e)
        {
            e.printStackTrace();
            Log.v("HTTP-REPLY", "CONDITION ERROR!!! ");
        }
        return oldUser;
    }

}
