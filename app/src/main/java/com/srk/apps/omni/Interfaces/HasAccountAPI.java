package com.srk.apps.omni.Interfaces;

import com.srk.apps.omni.SyncService.HasAccount;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * OMNI
 * by SRK on 25-01-2016 @ 14:13
 *
 * @SYNAPPS 2016
 */
public interface HasAccountAPI  {
    @GET("AuthUser.php?action=action&fb_ID=fb_ID")
    Call<HasAccount> getResult(
            @Query("action") String action,
            @Query("fb_ID") String fb_ID);
}