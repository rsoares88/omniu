package com.srk.apps.omni.SyncService;

import android.util.Log;

import com.srk.apps.omni.Krypton.Kryptonite;

/**
 * OMNI
 * by SRK on 22-01-2016 @ 23:33
 *
 * @SYNAPPS 2016
 */
public class HasService {

    public String action;
    public String result;

    public HasService(String action_value, String result_value) throws Exception {
        Log.e("KRYPTON", "ENCODED RESULT = " + result);
        this.action = action_value;
        this.result = result_value;

    }

    public String getResult() {
        try {
            Kryptonite mcrypt = new Kryptonite();
            String decrypted = mcrypt.decrypt(result);
            return decrypted;
        } catch (Exception e) {
            Log.e("KRYPTON", "FAILED TO DECODE RESULT = " + result);
            e.printStackTrace();
            return result;
        }
    }

}
