package com.srk.apps.omni.Activitys;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.srk.apps.omni.Dialogs.MainOptionsDialog;
import com.srk.apps.omni.Fragments.ChatWindow;
import com.srk.apps.omni.Fragments.EmptyFragment;
import com.srk.apps.omni.Fragments.ProfileViewOthers;
import com.srk.apps.omni.Fragments.ScanResults;
import com.srk.apps.omni.Fragments.UsersResults;
import com.srk.apps.omni.Fragments.messagesNotifications;
import com.srk.apps.omni.Helpers.NotificationsManager;
import com.srk.apps.omni.Helpers.UsersResultsList;
import com.srk.apps.omni.Interfaces.FriendsReplyAPI;
import com.srk.apps.omni.Interfaces.ScanResultAPI;
import com.srk.apps.omni.Krypton.Kryptonite;
import com.srk.apps.omni.MapsActivity;
import com.srk.apps.omni.R;
import com.srk.apps.omni.StorageClass.NotificationItem;
import com.srk.apps.omni.StorageClass.StoreMessage;
import com.srk.apps.omni.StorageClass.StorePrefs;
import com.srk.apps.omni.StorageClass.User;
import com.srk.apps.omni.StorageClass.UsersListItem;
import com.srk.apps.omni.SyncService.FriendsRequest;
import com.srk.apps.omni.SyncService.ScanedResults;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AppView extends AppCompatActivity implements View.OnTouchListener {

    public static AppView instance;
    protected Boolean DEBUG_MAPS = false;
    public User userdata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_view);
        instance = this;

        //get intent data
        Intent intent = getIntent();

        Bundle extras = intent.getExtras();
        String flag = extras.getString("flag");
        Integer fromID = extras.getInt("fromID");

        if (extras == null || flag.isEmpty()) {
            Log.e("APPVIEW", "NO INTENT EXTRAS BUNDLE FOUND!");
            flag = null; //SET FLAG TO NULL TO OPEN DEFAULT UI
        }

        Log.e("APPVIEW", "GOT FLAG: " + flag);
        switch (flag) {
            case "main_view": {
                userdata = (User) extras.getSerializable("userdata");
                Toast.makeText(this, "Welcome, " + userdata.FirstName + " " + userdata.LastName, Toast.LENGTH_SHORT).show();
                mainUI();
                break;
            }
            case "chat_window": {
                //ITS A CHAT WINDOW REQUEST, LOAD USERDATA
                //TODO VALIDATE CURRENT USER SESSION
                userdata = new StorePrefs().LoadUserPrefs(this);
                openChatWindow(extras);
                break;
            }
            case "friend_request":  {
                Log.e("TAG", "open accept user link request");
                userdata = new StorePrefs().LoadUserPrefs(instance);
                String from_username = "test";
                Integer from_ID = fromID;
                friendRequestDialog(from_username, fromID);
                //mainUI();
                break;
            }
            default: {
                userdata = new StorePrefs().LoadUserPrefs(instance);
                mainUI();
                break;
            }
        }

        //TODO OPEN DEBUG MAP FOR TESTING ONLY
        if (DEBUG_MAPS) {
            Intent maps_intent = new Intent(AppView.this, MapsActivity.class);
            startActivity(maps_intent);
        }
    }

    //initialize mainUI
    private CircularImageView profile_image;
    private FrameLayout appView;
    public FrameLayout toolBar;

    void openChatWindow(Bundle extras) {

        chatUI();
        UsersListItem fromUser = new UsersListItem();

        Integer fromID = extras.getInt("fromID");
        //read message from storedPrefs
        StorePrefs storage = new StorePrefs();
        StoreMessage stored_messages = storage.getMessagesFromID(this, fromID);
        //get last message

        fromUser.userID = stored_messages.from_ID;
        fromUser.username = stored_messages.from_username;
        fromUser.description = stored_messages.from_aboutU;
        fromUser.isFriend = stored_messages.isFriends;
        fromUser.profile_pic = null;

        Log.e("OPEN-CHAT", "OPEN CHAT WINDOW WITH: " + fromUser.username + "(" + fromUser.userID + ") FRIENDS: " + fromUser.isFriend);
        OpenUserChatWindow(fromUser);
    }

    public void friendRequestDialog(String from_username, Integer from_id) {
        Log.e("DIALOG", "link request from : " + from_username + " " + from_id);
        AlertDialog.Builder builder = new AlertDialog.Builder(instance);
        // Set the dialog title
        builder.setTitle("Link Request from: " + from_username);

        // Add the buttons
        builder.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                Log.e("TAG", "accepted link request");
                FriendRequestReply(true);
                mainUI();
            }
        });
        builder.setNegativeButton("Deny", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                Log.e("TAG", "refused link request");
                FriendRequestReply(false);
                mainUI();
            }
        });
// Set other dialog properties

// Create the AlertDialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    void FriendRequestReply(Boolean reply) {

        if(userdata == null) {
            userdata = new StorePrefs().LoadUserPrefs(this);
        }

        String replyString = reply.toString();

        String API_BASE_URL = getResources().getString(R.string.API_BASE_URL);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        String cryted_Action = "Friend_Reply";
        // Create an instance of our API interface.
        FriendsReplyAPI service = retrofit.create(FriendsReplyAPI.class);
        Call<FriendsRequest> call = service.getResult(cryted_Action, userdata.UserID, 2, replyString); //

        call.enqueue(new Callback<FriendsRequest>() {
            @Override
            public void onResponse(Response<FriendsRequest> response) {
                Log.e("Response code ", String.valueOf(response.code()));
                String value = response.body().result;
                Log.e("KRYPTON-RESULT", "DECODED VALUE: " + value);
                if (value == null) {
                    Log.e("RESULT", "GOT NULL RESPONSE");
                    return;
                }
                switch (value) {
                    case "true": {
                        Log.i("FRIENDS-RESULT", "FRIEND REPLY RESULT: " + response.body().result + " LIST: " + response.body());
                        //Toast.makeText(AppView.instance, "Link request sent to " + toUser.username, Toast.LENGTH_LONG).show();
                        break;
                    }
                    case "fail": {
                        Log.i("FRIENDS-RESULT", "FAILED RESULT: " + value);
                        break;
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("FAILURE-SCANNING-RESULT", String.valueOf(t));
                System.out.println(t.getMessage());
                System.out.println(t.getCause());
            }
        });

    }


    void InitToolBar() {
        initialized_toolbar = true;
        profile_image = (CircularImageView) findViewById(R.id.scanButton);
        toolBar = (FrameLayout) findViewById(R.id.toolBar);
        profile_image.setOnTouchListener(this);
    }

    void HideToolbar() {
        toolBar = (FrameLayout) findViewById(R.id.toolBar);
        toolBar.setVisibility(View.GONE);
    }

    void ShowToolbar() {
        if (!initialized_toolbar) {
            mainUI();
        }
        toolBar = (FrameLayout) findViewById(R.id.toolBar);
        UpdateNotifications();
        toolBar.setVisibility(View.VISIBLE);
    }

    void mainUI() {
        if (instance == null) {
            instance = this;
    }
        //get all view widgets
        InitToolBar();
        appView = (FrameLayout) findViewById(R.id.appView);

        // load user avatar
        if (userdata != null) {
            //Toast.makeText(this, "avatar, " + userdata.Avatar_Img_Link, Toast.LENGTH_SHORT).show();
            //profile_image
            String API_BASE_URL = getApplicationContext().getString(R.string.AVATAR_IMG_BASE_URL);
            String Avatar_Img_Link = userdata.Avatar_Img_Link;
            String final_path = API_BASE_URL + Avatar_Img_Link;

            Picasso.with(instance)
                    .load(final_path)
                    .placeholder(R.drawable.ic_placeholder) // optional
                    .error(R.drawable.ic_error_fallback)         // optional
                    .into(profile_image);
        }
        doSetEmptyFragment("end_scan");
        //finished loading UI
        if (isFirst_run) {
            startScan(profile_image);
            isFirst_run = false;
        }
        UpdateNotifications();
    }

    void chatUI() {
        appView = (FrameLayout) findViewById(R.id.appView);
        doSetEmptyFragment("loading_chat");
        HideToolbar();
    }

    private Boolean ALREADY_SCANNING = false;
    private Boolean isFirst_run = true;

    public void startScan(View view) {
        Log.d("APP", "doing scan");
        // Obtain MotionEvent object
        long downTime = SystemClock.uptimeMillis();
        long eventTime = SystemClock.uptimeMillis() + 100;
        float x = 0.0f;
        float y = 0.0f;

        // List of meta states found here: developer.android.com/reference/android/view/KeyEvent.html#getMetaState()
        int metaState = 0;
        MotionEvent motionEvent_DOWN = MotionEvent.obtain(
                downTime,
                eventTime,
                MotionEvent.ACTION_DOWN,
                x,
                y,
                metaState
        );
        onTouch(view, motionEvent_DOWN);

        MotionEvent motionEvent_UP = MotionEvent.obtain(
                downTime,
                eventTime,
                MotionEvent.ACTION_UP,
                x,
                y,
                metaState
        );
        onTouch(view, motionEvent_UP);
        profile_image.setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Log.e("ONTOUCH", "TOUCHID: " + v.getId() + " / " + profile_image.getId());
        if (scannedResults == null) {
            resetCheckPoints();
        } else {
            if (scannedResults.body().usersCount <= 0) {
                resetCheckPoints();
            }
        }

        //if scan button was pressed
        if (profile_image.getId() == v.getId() && !ALREADY_SCANNING) {
            Log.e("ONTOUCH", "TOUCHID MATCHED");
            //closeScanFragment();
            doSetEmptyFragment("start_scan");

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    if(instance == null) { break; }
                    int color = R.color.selected_categories; //your color
                    int intColor = ContextCompat.getColor(instance, color);
                    //profile_image.setBorderColor(0x77000000);
                    profile_image.setBorderColor(intColor);
                    profile_image.invalidate();
                    break;
                }
                case MotionEvent.ACTION_UP: {
                    int color = R.color.blueAccent; //your color
                    int intColor = ContextCompat.getColor(this, color);
                    profile_image.setBorderColor(intColor);
                    profile_image.invalidate();

                    final ImageView imgScan = (ImageView) findViewById(R.id.imgScan);

                    final AnimationSet animation_set = new AnimationSet(true);

                    ScaleAnimation fade_in = new ScaleAnimation(0f, 1000f, 0f, 1000f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                    fade_in.setDuration(1600);     // animation duration in milliseconds
                    fade_in.setFillAfter(false);

                    Animation fade_out = new AlphaAnimation((float) 1, 0);
                    fade_out.setDuration(1600);

                    animation_set.addAnimation(fade_in);
                    animation_set.addAnimation(fade_out);

                    animation_set.setFillAfter(false);

                    imgScan.startAnimation(animation_set);

                    animation_set.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation arg0) {
                            Log.e("CHECKPOINTS",
                                    "isFinishedScanning " + instance.isFinishedScanning +
                                            " instance.isParsingScanResults " + instance.isParsingScanResults +
                                            " instance.isFinishedScanning " + instance.isFinishedScanning +
                                            " instance.ALREADY_SCANNING " + instance.ALREADY_SCANNING +
                                            " instance.gotScanResults " + instance.gotScanResults +
                                            " gotScanResults " + gotScanResults
                            );

                            if (!ALREADY_SCANNING) {
                                ALREADY_SCANNING = true;
                                GetScanResults();
                            }
                        }

                        @Override
                        public void onAnimationEnd(Animation arg0) {
                            //if scan results is not finished, repeat animation
                            if (!gotScanResults) {
                                imgScan.startAnimation(animation_set);
                                //if has scan results, start parsing it, until isFinishedScanning
                            } else {
                                //start parsing
                                if (!isParsingScanResults) {
                                    doScanResults();
                                } else {
                                    //finished parsing results, reset Booleans
                                    if (isFinishedScanning) {
                                        resetCheckPoints();
                                    }
                                    //still parsing values, repeat animations
                                    else {
                                        imgScan.startAnimation(animation_set);
                                    }
                                }
                            }
                            //ANIMATION ENDED
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {
                            //empty stub method
                        }
                    });
                    break;
                }
            }
        }
        //RETURN TRUE SO THE APP CAN STILL PASS TOUCH EVENTS TO OTHER OBJECTS
        return true;
    }

    EmptyFragment Empty_Fragment;

    void doSetEmptyFragment(String action) {
        Empty_Fragment = new EmptyFragment();

        Bundle args = new Bundle();
        args.putString("action", action);
        Empty_Fragment.setArguments(args);

        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(appView.getId(), Empty_Fragment, "EmptyFragment");
        // Commit the transaction
        transaction.commit();
    }

    Boolean gotScanResults = false;
    int RETRY_COUNTER = 0;
    boolean GetScanResults() {
        doSetEmptyFragment("start_scan");
        //do retrofit request for closest points of interest
        String API_BASE_URL = getResources().getString(R.string.API_BASE_URL);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Kryptonite krypton = new Kryptonite();
        String encrypted = null;
        try {
            encrypted = Kryptonite.bytesToHex(krypton.encrypt("closest_Points"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        String cryted_Action = encrypted;
        // Create an instance of our API interface.
        ScanResultAPI service = retrofit.create(ScanResultAPI.class);
        Call<ScanedResults> call = service.getScan(cryted_Action, userdata.UserID);

        call.enqueue(new Callback<ScanedResults>() {
            @Override
            public void onResponse(Response<ScanedResults> response) {
                Log.e("Response code ", String.valueOf(response.code()));
                String value = response.body().result;
                Log.e("KRYPTON-RESULT", "DECODED VALUE: " + value);
                if (value == null) {
                    Log.e("RESULT", "GOT NULL RESPONSE");
                    return;
                }
                switch (value) {
                    case "true": {
                        gotScanResults = true;
                        scannedResults = response;
                        Log.i("SCAN-RESULT", "GOT SCAN RESULTS: " + response.body().usersCount + " LIST: " + response.body().rawList);
                        break;
                    }
                    case "fail": {
                        Log.i("SCAN-RESULT", "FAILED SCAN: " + value);
                        RETRY_COUNTER++;
                        if(RETRY_COUNTER > 5) {
                            gotScanResults = true;
                            scannedResults = response;
                        }
                        break;
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("FAILURE-SCANNING-RESULT", String.valueOf(t));
                System.out.println(t.getMessage());
                System.out.println(t.getCause());
                gotScanResults = true;
                //scannedResults = "null";
            }
        });
        return true;
    }

    void resetCheckPoints() {
        isParsingScanResults = false;
        isFinishedScanning = false;
        ALREADY_SCANNING = false;
        gotScanResults = false;
    }

    public static Response<ScanedResults> scannedResults;

    Boolean isFinishedScanning = false;
    Boolean isParsingScanResults = false;
    android.support.v4.app.FragmentTransaction transaction;

    void doScanResults() {
        isParsingScanResults = true;
        // NOW OPEN SCANNED RESULTS, IF NO RESULTS ARE FOUND, DISPLAY MESSAGE
        if(scannedResults.body().usersCount == null) {
            isFinishedScanning = true;
            doSetEmptyFragment("no_results");
            return;
        }
        Integer usersCount = scannedResults.body().usersCount;

        if (usersCount <= 0) {
            //found no results
            //change EmptyFragment msg and hide progressScan
            isFinishedScanning = true;
            doSetEmptyFragment("no_results");
            return;
        }

        //CLOSE PREVIEWS FRAGMENTS
        doSetEmptyFragment("end_scan");
        //do retrofit request for colosest pints of interest
        ScanResults scanResults_Fragment = new ScanResults();
        Bundle args = new Bundle();
        args.putInt("usersCount", usersCount);
        args.putString("rawList", scannedResults.body().rawList);
        args.putIntArray("usersFound", scannedResults.body().getUsersList());

        //int[] new_array = scannedResults.body().getUsersList();
        //Log.e("RESULT", "length: " + new_array.length + " full array: " + scannedResults.body().rawList);

        scanResults_Fragment.setArguments(args);
        isFinishedScanning = true;
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(appView.getId(), scanResults_Fragment, "scanResults");
        // Commit the transaction
        transaction.commit();
        resetCheckPoints();
    }

    String LastRawValues;

    public void OpenUserResultsFragment(String rawValues) {
        LastRawValues = rawValues;
        UsersResults UsersResults_Fragment = new UsersResults();
        Bundle args = new Bundle();
        args.putString("rawList", rawValues);
        args.putInt("userID", userdata.UserID);
        UsersResults_Fragment.setArguments(args);

        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(appView.getId(), UsersResults_Fragment, "usersResults");
        // Commit the transaction
        transaction.commit();
    }

    public void OpenUserChatWindow(UsersListItem toUser) {
        ChatWindow ChatWindow_Fragment = new ChatWindow();
        Bundle args = new Bundle();
        args.putSerializable("toUser", toUser);
        args.putString("flag", "IM");

        ChatWindow_Fragment.setArguments(args);
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(appView.getId(), ChatWindow_Fragment, "ChatWindow");
        // Commit the transaction
        transaction.commit();
        HideToolbar();
    }

    public void OpenUserChatWindow(NotificationItem notification) {
        //create UsersListItem
        UsersListItem toUser = new UsersListItem();
        //public Integer userID;
        //public String username;
        //public String description;
        //public Integer isFriend;
        //public String profile_pic;

        toUser.userID = notification.fromUserID;
        toUser.username = notification.Title;
        toUser.description = notification.Desc;
        toUser.isFriend = 0;
        toUser.profile_pic = notification.Avatar_pic;
        OpenUserChatWindow(toUser);
    }

    public void OpenViewUserProfile(UsersListItem toUser) {
        ProfileViewOthers ProfileView_Fragment = new ProfileViewOthers();
        Bundle args = new Bundle();
        args.putSerializable("toUser", toUser);
        //args.putString("flag", "IM");

        ProfileView_Fragment.setArguments(args);
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(appView.getId(), ProfileView_Fragment, "ViewProfile");
        transaction.addToBackStack("ViewProfile");
        // Commit the transaction
        transaction.commit();

    }

    public void OpenMessagesListFragment() {
        Log.e("USERS-RES", "OPEN MESSAGES LIST");
        messagesNotifications messagesNotified = new messagesNotifications();
        Bundle args = new Bundle();
        NotificationsManager notifications = new StorePrefs().LoadNotifications(instance);
        args.putSerializable("NotifiedList", notifications);
        //args.putString("flag", "IM");

        messagesNotified.setArguments(args);
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(appView.getId(), messagesNotified, "ListMessages");
        transaction.addToBackStack("ListMessages");
        // Commit the transaction
        transaction.commit();
    }

    boolean initialized_toolbar = false;

    private Boolean USER_BACK_PRESSED = false;
    private final Runnable exit_mRunnable = new Runnable() {
        @Override
        public void run() {
            USER_BACK_PRESSED = false;
        }
    };
    private Toast exit_toast;
    private Handler exit_mHandler = new Handler();

    @Override
    public void onBackPressed() {
        Log.e("BPRESS", "BACKPRES: " + LastRawValues + " : " + ChatWindow.instance);
        //IF USER AS A CHAT WINDOW OPEN
        if (ChatWindow.instance != null) {
            ChatWindow.instance = null;
            if (LastRawValues != null) {
                OpenUserResultsFragment(LastRawValues);
                ShowToolbar();
            } else {
                //super.onBackPressed();
                ShowToolbar();
                doSetEmptyFragment("end_scan");
            }
            return;
        }

        //IF USER AS SCANNED RESULTS OPEN
        if (ScanResults.instance != null) {
            ScanResults.instance = null;
            doSetEmptyFragment("end_scan");
            if (UsersResultsList.instance != null) {
                UsersResultsList.instance = null;
            }
            return;
        }

        //USER IS @ A BLANK PAGE
        if (EmptyFragment.instance != null) {
            Log.d("PRESS", "TODO EMPTY ISTANCE LOADED, NO SCANRESULTS");
            //TODO DOUBLE TAP TO EXIT APP
            if (USER_BACK_PRESSED) {
                if (this.exit_toast != null)
                    this.exit_toast.cancel();
                //super.onBackPressed();
                if(MainActivity.mainactivity != null) {
                    MainActivity.mainactivity.finish();
                }
                this.finish();
                return;
            }
            this.exit_toast = Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT);
            this.USER_BACK_PRESSED = true;
            this.exit_toast.show();
            exit_mHandler.postDelayed(exit_mRunnable, 2000);
            return;
        } else {
            //SHOULD NEVER GET HERE
            Log.d("PRESS", "TODO EMPTY ISTANCE WAS EMPTY, WTF???");
            super.onBackPressed();
            return;
        }

    }

    @Override
    public void onDestroy()
    {
        // RUN SUPER | REGISTER ACTIVITY AS NULL IN APP CLASS
        this.instance = null;
        super.onDestroy();
    }

    TextView notifyCount;
    public void UpdateNotifications() {

        NotificationsManager notifications = new StorePrefs().LoadNotifications(this);
        if(notifications == null) {
            //instance.notifyCount.setVisibility(View.GONE);
            ToggleNotificationCount(0);
            return;
        }

        Integer notificationsCount = notifications.NotificationsList.size();
        ToggleNotificationCount(notificationsCount);

    }

    void ToggleNotificationCount(final Integer notificationsCount) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (notifyCount == null) {
                    notifyCount = (TextView) findViewById(R.id.notifyCount);
                }
                //Your code to run in GUI thread here
                if (notificationsCount <= 0) {
                    notifyCount.setVisibility(View.GONE);
                    return;
                } else {
                    notifyCount.setVisibility(View.VISIBLE);
                    notifyCount.setText(notificationsCount.toString());
                }
                notifyCount.setText(notificationsCount.toString());
            }//public void run() {
        });
    }

    public void openOptionsDialog(View view) {
        MainOptionsDialog dialog = new MainOptionsDialog();
        dialog.show(getFragmentManager(), "Options Menu");
        //showPopup(view);
    }

    public void EditUserProfile(View view) {
        Log.e("DEBUG", "I CLICKED OPEN EDIT PROFILE VIEW");
    }

    @Deprecated
    public void showPopup(View anchorView) {

        View popupView = getLayoutInflater().inflate(R.layout.options_dialog, null);

        PopupWindow popupWindow = new PopupWindow(popupView,
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        // Example: If you have a TextView inside `popup_layout.xml`
        //TextView tv = (TextView) popupView.findViewById(R.id.);

        //tv.setText(....);

        // Initialize more widgets from `popup_layout.xml`
        //....
        //....

        // If the PopupWindow should be focusable
        popupWindow.setFocusable(true);

        // If you need the PopupWindow to dismiss when when touched outside
        popupWindow.setBackgroundDrawable(new ColorDrawable());

        int location[] = new int[2];

        // Get the View's(the one that was clicked in the Fragment) location
        anchorView.getLocationOnScreen(location);

        // Using location, the PopupWindow will be displayed right under anchorView
        popupWindow.showAtLocation(anchorView, Gravity.NO_GRAVITY,
                location[0], location[1] + anchorView.getHeight());

    }

}
