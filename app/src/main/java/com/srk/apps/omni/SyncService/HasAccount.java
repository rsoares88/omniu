package com.srk.apps.omni.SyncService;

import android.util.Log;

import com.srk.apps.omni.Krypton.Kryptonite;

/**
 * OMNI
 * by SRK on 25-01-2016 @ 14:12
 *
 * @SYNAPPS 2016
 */
public class HasAccount {
    public String action;
    public String result;

    public String getResult() {
        try {
            Kryptonite mcrypt = new Kryptonite();
            String decrypted = mcrypt.decrypt(result);
            return decrypted;
        } catch (Exception e) {
            Log.e("KRYPTON", "FAILED TO DECODE RESULT = " + result);
            e.printStackTrace();
            return result;
        }
    }
}
