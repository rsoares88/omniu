package com.srk.apps.omni.StorageClass;


import java.io.Serializable;

/**
 * Created by SRK on 17-01-2016.
 */
public class User implements Serializable {

    public String SessionID = null;
    public Integer profile_ID = 0;
    public String FirstName = null;
    public String LastName = null;
    public Integer Gender = null;
    public String Username = null;
    public String Email = null;
    public String Password =null;
    public Integer UserID = 0;
    public String FacebookID = null;
    public Boolean isFacebook = false;
    public String BirthDate = null;

    public Integer hiden_avatar = 0;
    public String Cover_Img_Link = null;
    public String Avatar_Img_Link = null;

    public String AboutU = null;
    public String SelectedInterests;

}