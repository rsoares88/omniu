package com.srk.apps.omni.Helpers;

import android.content.Context;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.srk.apps.omni.Fragments.ChatWindow;
import com.srk.apps.omni.R;
import com.srk.apps.omni.StorageClass.ChatMessage;
import com.srk.apps.omni.StorageClass.StorePrefs;

import java.util.ArrayList;
import java.util.List;

/**
 * OMNI
 * by SRK on 07-02-2016 @ 02:04
 *
 * @SYNAPPS 2016
 */
public class ChatArrayAdapter extends ArrayAdapter<ChatMessage> {
    Context context;
    public static ChatArrayAdapter instance;

    private List<ChatMessage> chatMessageList = new ArrayList<>();

    private boolean isCleared = false;

    public void setChatMessageList(List<ChatMessage> messages) {
        //chatMessageList = messages;
        chatMessageList = new ArrayList<>();
        for (ChatMessage item : messages) {
            isCleared = false;
            instance.add(item);
        }

        if (getCount() <= 0 && !isCleared) {
            isCleared = true;
            instance.clear();
        }

        StorePrefs storage = new StorePrefs();
        NotificationsManager notifications = storage.LoadNotifications(context);
        //notifications.removeItem(context, messages.);
        for (ChatMessage messag:messages) {
            notifications.removeItem(context, messag.uniqueID);
        }

    }

    public ChatArrayAdapter(Context context, int resourceId) {
        super(context, resourceId);
        this.context = context;
        this.instance = this;
    }

    @Override
    public void add(ChatMessage message) {
        chatMessageList.add(message);
        super.add(message);
    }

    public int getCount() {
        return this.chatMessageList.size();
    }

    public ChatMessage getItem(int index) {
        return this.chatMessageList.get(index);
    }

    private class ViewHolder {
        TextView message_text;
        TextView readTimer;
        LinearLayout itemRow;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ChatMessage chatMessageObj = getItem(position);

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.chat_message_row, parent, false);
            convertView.setTag(holder);
            holder.itemRow = (LinearLayout) convertView.findViewById(R.id.itemRow);
            holder.message_text = (TextView) convertView.findViewById(R.id.message_text);
            holder.readTimer = (TextView) convertView.findViewById(R.id.readTimer);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (String.valueOf(chatMessageObj.readTime).equals(null)) {
            chatMessageObj.readTime = 0;
        }

        holder.message_text.setText(chatMessageObj.getMessage());
        holder.readTimer.setText(String.valueOf(chatMessageObj.readTime));

        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) holder.message_text.getLayoutParams();
        if (chatMessageObj.left) {
            holder.message_text.setBackgroundResource(R.drawable.bubble_chat_left);
            lp.gravity = Gravity.LEFT;
        } else {
            holder.message_text.setBackgroundResource(R.drawable.bubble_chat_rigth);
            lp.gravity = Gravity.RIGHT;
        }
        holder.itemRow.setGravity(lp.gravity);
        //holder.readTimer.setLayoutParams(lp);
        //holder.message_text.setLayoutParams(lp);
        //holder.itemRow.setLayoutParams(lp);
        //LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        startSelfDestruct();
        return convertView;
    }

    boolean isCounting = false;

    void startSelfDestruct() {
        //creates a timer that updates the readTime in the message object
        if (!isCounting) {
            isCounting = true;
            handler.postDelayed(runnable, interval);
        }
    }

    private final int interval = 1000; // 1 Second
    public static final int max_interval = 15;
    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        public void run() {
            if (ChatWindow.instance != null && getCount() > 0) {
                //foreach message update readTime
                for (ChatMessage item : chatMessageList) {
                    StorePrefs storage = new StorePrefs();
                    Context getCtx = getContext();
                    if(getCtx != null) {
                        storage.updateMessageCounter(ChatWindow.instance.getContext(), item.fromUser, item.uniqueID);
                    }
                }
                handler.postDelayed(runnable, interval);
            } else {
                isCounting = false;
            }
            if (ChatWindow.instance != null) {
                ChatWindow.instance.UpdateArrayAdapter();
            }
        }
    };

}
