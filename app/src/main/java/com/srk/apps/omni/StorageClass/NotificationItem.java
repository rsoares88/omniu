package com.srk.apps.omni.StorageClass;

import java.io.Serializable;

/**
 * OMNI
 * by SRK on 12-02-2016 @ 17:12
 *
 * @SYNAPPS 2016
 */
public class NotificationItem implements Serializable {
    public NotificationItem(StoreMessage item) {

        if(item.flag == "friend_request") {
            this.Title = "Link request from: " + item.from_username;
            this.fromUserID = item.from_ID;
            this.Desc = item.from_username + " wants to link with you.";
        } else {
            this.Title = item.from_username;
            this.Desc = item.getLastMsg().getMessage();
            this.Time = item.getLastMsg().TimeStamp;
            this.fromUserID = item.getLastMsg().fromUser;
            this.UniqueID = item.getLastMsg().uniqueID;
            this.Avatar_pic = item.user_avatar;
            this.Cover_pic = item.user_cover;
        }

        this.Flag = item.flag;

    }

    public String Title;
    public String Desc;
    public String Time;
    public String Flag;
    public String UniqueID;
    public Integer fromUserID;
    public String Avatar_pic;
    public String Cover_pic;

}
