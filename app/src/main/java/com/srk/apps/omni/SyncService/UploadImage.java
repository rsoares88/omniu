package com.srk.apps.omni.SyncService;

import android.os.AsyncTask;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.srk.apps.omni.Activitys.Create_Profile;
import com.srk.apps.omni.Helpers.ImagesCompress;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Arrays;

/**
 * OMNI
 * by SRK on 24-01-2016 @ 23:36
 *
 * @SYNAPPS 2016
 */
public class UploadImage extends AsyncTask<String, Void, String> {

    private String[] IMAGE_EXTENSIONS = {"png", "jpg", "jpeg", "bmp"};


    public static String getExtension(String fileName) {
        String encoded;
        try {
            encoded = URLEncoder.encode(fileName.toLowerCase(), "UTF-8").replace("+", "%20");
        } catch (UnsupportedEncodingException e) {
            encoded = fileName.toLowerCase();
        }
        return MimeTypeMap.getFileExtensionFromUrl(encoded).toLowerCase();
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            String sourceFileUri = params[0];
            String action = params[1];
            String ownerID = params[2];
            Log.d("progress", "FilePath: " + sourceFileUri + "type: " + getExtension(sourceFileUri) + "action: " + action);

            HttpURLConnection conn = null;
            DataOutputStream dos = null;
            String lineEnd = "\r\n";
            String twoHyphens = "--";
            String boundary = "*****";
            int bytesRead, bytesAvailable, bufferSize;
            byte[] buffer;
            int maxBufferSize = 1 * 1024 * 1024;


            switch (action) {
                case "avatar_upload": {

                }
                case "cover_upload": {

                }
                case "gallery_upload": {

                }
            }

            File sourceFile = new File(sourceFileUri);

            if(!sourceFile.isFile()) {
                //needs to create the file
            }


            Log.d("progress", "isFile: " + sourceFile.isFile() + " type: " + getExtension(sourceFileUri));
            if (sourceFile.isFile()) {
                if (Arrays.asList(IMAGE_EXTENSIONS).contains(getExtension(sourceFileUri))) //IMAGE_EXTENSIONS - getExtension(sourceFileUri)
                {
                    //CHECK IF COMPRESSION IS NEEDED
                    sourceFile = new File(ImagesCompress.compressImage(sourceFileUri));
                }


                try {

                    String IMG_BASE_URL = "http://activetest.pcriot.com/omniu/UploadData.php?ownerID=" + ownerID;//Resources.getSystem().getString(R.string.API_IMG_BASE_URL);
                    String upLoadServerUri = IMG_BASE_URL;
                    //String upLoadServerUri = "http://activetest.pcriot.com/omniu/UploadData.php?";

                    // open a URL connection to the Servlet
                    FileInputStream fileInputStream = new FileInputStream(sourceFile);
                    URL url = new URL(upLoadServerUri);

                    // Open a HTTP connection to the URL
                    conn = (HttpURLConnection) url.openConnection();
                    conn.setDoInput(true); // Allow Inputs
                    conn.setDoOutput(true); // Allow Outputs
                    conn.setUseCaches(false); // Don't use a Cached Copy
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Connection", "Keep-Alive");
                    conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                    conn.setRequestProperty("Content-Type",
                            "multipart/form-data;boundary=" + boundary);

                    conn.setRequestProperty(action, sourceFileUri);
                    dos = new DataOutputStream(conn.getOutputStream());

                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes("Content-Disposition: form-data; name=\""+ action +"\";filename=\""
                            + sourceFileUri + "\"" + lineEnd);

                    dos.writeBytes(lineEnd);

                    // create a buffer of maximum size
                    bytesAvailable = fileInputStream.available();

                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    buffer = new byte[bufferSize];

                    // read file and write it into form...
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                    while (bytesRead > 0) {
                        dos.write(buffer, 0, bufferSize);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fileInputStream.read(buffer, 0,
                                bufferSize);
                    }
                    // send multipart form data necesssary after file data...
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens
                            + lineEnd);
                    Log.e("UPLOAD", "URL: " + conn.getURL());
                    // Responses from the server (code and message)
                    int serverResponseCode = conn.getResponseCode();
                    String serverResponseMessage = conn
                            .getResponseMessage();
                    Log.d("UPLOAD", "upoloading result " + serverResponseCode + " resultmsg: "
                            + serverResponseMessage);
                    if(serverResponseCode == 200) {
                        //upload as finished, do actions
                        Log.d("UPLOAD", "upoloading success " + serverResponseCode + " resultmsg: "
                                + serverResponseMessage);
                        // recursiveDelete(mDirectory1);

                        switch (action) {
                            case "avatar_upload": {
                                //AVATAR UPLOAD FINISHED
                                Log.d("UPLOAD", "FINISHED: avatar_upload FLNAME: " + sourceFile.getName());
                                Create_Profile.create_profile.Avatar_UploadFinished();
                                break;
                            }
                            case "cover_upload": {
                                //COVER UPLOAD FINISHED
                                Log.d("UPLOAD", "FINISHED: cover_upload FLNAME: " + sourceFile.getName());
                                Create_Profile.create_profile.Cover_UploadFinished();
                                break;
                            }
                            case "gallery_upload": {
                                break;
                            }
                        }

                    }
                    // close the streams
                    fileInputStream.close();
                    dos.flush();
                    dos.close();
                } catch (Exception e) {
                    // dialog.dismiss();
                    e.printStackTrace();
                }
                // dialog.dismiss();
            } // End else block
        } catch (Exception ex) {
            // dialog.dismiss();
            ex.printStackTrace();
        }
        return "Executed";
    }

    @Override
    protected void onPostExecute(String result) {
        Log.d("UPLOAD-FINISHED", ": " + result);
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        Log.d("UPLOAD-PROGRESS", "onProgressUpdate: " + values);
    }

}
