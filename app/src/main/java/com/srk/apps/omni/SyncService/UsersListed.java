package com.srk.apps.omni.SyncService;

import android.util.Log;

import com.srk.apps.omni.StorageClass.UsersListItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * OMNI
 * by SRK on 01-02-2016 @ 23:46
 *
 * @SYNAPPS 2016
 */
public class UsersListed {

    public String action;
    public String result;
    public String rawValues;

    public List<UsersListItem> getListedUsers() {
        JSONArray resultObject;
        List<UsersListItem> fakeList = new ArrayList<>();
        try {
            resultObject = new JSONArray(rawValues);
            for (int i=0; i<resultObject.length(); i++) {
                JSONObject values = new JSONObject(resultObject.getString(i));
                Log.e("GSON", "USERNAME: " + values.getString("username"));
                UsersListItem parsed = new UsersListItem();
                parsed.userID = values.getInt("userID");
                parsed.username = values.getString("username");
                parsed.description = values.getString("description");
                parsed.isFriend = values.getInt("isFriend");
                parsed.profile_pic = values.getString("profile_pic");
                parsed.hidden_Avatar = values.getInt("hidden_Avatar");
                fakeList.add(parsed);
            }
        } catch (JSONException e) {
                e.printStackTrace();
                Log.v("JSON-REPLY", "CONDITION ERROR!!! " + e.getCause());
            }
        //todo debug list
        return fakeList;
    }

}
