package com.srk.apps.omni.StorageClass;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * OMNI
 * by SRK on 08-02-2016 @ 13:44
 *
 * @SYNAPPS 2016
 */
public class StoredMessages implements Serializable {

    public List<StoreMessage> MessagesStored = new ArrayList<>();

}
