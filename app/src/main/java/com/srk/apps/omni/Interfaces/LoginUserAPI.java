package com.srk.apps.omni.Interfaces;

import com.srk.apps.omni.SyncService.LoginUser;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * OMNI
 * by SRK on 25-01-2016 @ 18:01
 *
 * @SYNAPPS 2016
 */
public interface LoginUserAPI {
    @GET("AuthUser.php?action=action&username=username&password=password&deviceID=deviceID&instanceID=instanceID")
    Call<LoginUser> getResult(
            @Query("action") String action,
            @Query("username") String username,
            @Query("password") String password,
            @Query("deviceID") String deviceID,
            @Query("instanceID") String instanceID);
}
