package com.srk.apps.omni.Helpers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.srk.apps.omni.Activitys.Create_Profile;
import com.srk.apps.omni.R;

import java.util.List;

/**
 * OMNI
 * by SRK on 19-01-2016 @ 02:46
 *
 * @SYNAPPS 2016
 */
public class CategoriesGrid extends BaseAdapter {

    private Context mContext;
    private final int[] Imageid;
    private final List SelectedItems;

    private static LayoutInflater inflater = null;

    public CategoriesGrid(Context c, int[] Imageid, List selectedItems) {
        mContext = c;

        this.Imageid = AvatarsList.ImageId;
        this.SelectedItems = selectedItems;

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return Imageid.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return (long) position;
    }

    static class ViewHolderItem {

        CircularImageView avatar;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolderItem viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Create_Profile.create_profile.getApplicationContext().LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.categ_grid_single, null);
            viewHolder = new ViewHolderItem();

            viewHolder.avatar = (CircularImageView) convertView.findViewById(R.id.grid_image);
            // store the holder with the view.
            convertView.setTag(viewHolder);

            //Log.d("LIST-ITEMS", "ITEM-ID: " + position);
        } else {
            viewHolder = (ViewHolderItem) convertView.getTag();
        }

        viewHolder.avatar.setImageResource(Imageid[position]);
        //ImageView more = (ImageView)grid.findViewById(R.id.grid_image);



            /*
            if(Create_Profile.create_profile.SelectedValues.contains(getItemId(position))) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    more.getBackground().setColorFilter(mContext.getResources().getColor(R.color.selected_categories), PorterDuff.Mode.SRC_IN);
                } else {
                    Drawable wrapDrawable = DrawableCompat.wrap(grid.getBackground());
                    DrawableCompat.setTint(wrapDrawable, mContext.getResources().getColor(R.color.selected_categories));
                    more.setBackgroundDrawable(DrawableCompat.unwrap(wrapDrawable));
                }
            }
*/
        //} else {
        //    grid = (View)convertView;
        // }

        return convertView;
    }
}
