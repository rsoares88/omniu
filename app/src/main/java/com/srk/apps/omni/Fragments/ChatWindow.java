package com.srk.apps.omni.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.srk.apps.omni.Activitys.AppView;
import com.srk.apps.omni.Activitys.MainActivity;
import com.srk.apps.omni.Helpers.ChatArrayAdapter;
import com.srk.apps.omni.Interfaces.SendMessageAPI;
import com.srk.apps.omni.Krypton.Kryptonite;
import com.srk.apps.omni.R;
import com.srk.apps.omni.StorageClass.ChatMessage;
import com.srk.apps.omni.StorageClass.StoreMessage;
import com.srk.apps.omni.StorageClass.StorePrefs;
import com.srk.apps.omni.StorageClass.User;
import com.srk.apps.omni.StorageClass.UsersListItem;
import com.srk.apps.omni.SyncService.SentMessage;

import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatWindow extends Fragment {

    public static ChatWindow instance;

    public ChatWindow() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.instance = this;
    }

    View view_instance;

    ListView listView;
    ChatArrayAdapter chatArrayAdapter;

    TextView userName;
    TextView aboutU;

    EditText textMsg;
    Button btn_sendMsg;

    User userdata;
    UsersListItem toUser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view_instance = inflater.inflate(R.layout.fragment_chat_window, container, false);
        //layout.getWindowId(); //setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        //getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        Bundle args = getArguments();

        if (args != null && view_instance != null) {
            userdata = AppView.instance.userdata;

            listView = (ListView) view_instance.findViewById(R.id.listView);
            chatArrayAdapter = new ChatArrayAdapter(view_instance.getContext(), R.layout.chat_message_row);
            listView.setAdapter(chatArrayAdapter);

            userName = (TextView) view_instance.findViewById(R.id.userName);
            aboutU = (TextView) view_instance.findViewById(R.id.aboutU);

            textMsg = (EditText) view_instance.findViewById(R.id.textMsg);

            btn_sendMsg = (Button) view_instance.findViewById(R.id.btn_sendMsg);

            String action = args.getString("flag");
            switch (action) {
                //GOT IM => PRIVATE MESSAGE
                case "IM": {
                    toUser = (UsersListItem) args.getSerializable("toUser");
                    UpdateArrayAdapter();
                    //userName
                    userName.setText(toUser.username);
                    aboutU.setText(toUser.description);
                    btn_sendMsg.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View arg0) {
                            // TODO Auto-generated method stub
                            if (!textMsg.getText().toString().isEmpty()) {
                                SendMessage(textMsg.getText().toString());
                                btn_sendMsg.setEnabled(false);
                            }
                        }
                    });
                    break;
                }
            }
        }
        return view_instance;
    }

    void SendMessage(final String msg) {
       // Log.e("SEND MESSAGE", "FROM: " + userdata.UserID + "TO: " + toUser.username + " ID: " + toUser.userID + " MSG: " + msg);
        String API_BASE_URL = getResources().getString(R.string.API_BASE_URL);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Kryptonite mcrypt = new Kryptonite();
        String encrypted = null;
        String krypted_msg = msg;
        try {
            encrypted = Kryptonite.bytesToHex(mcrypt.encrypt("send_message"));
            //TODO ENCRYPT MSGS
            krypted_msg = msg;
        } catch (Exception e) {
            e.printStackTrace();
        }

        String cryted_Action = encrypted;
        // Create an instance of our API interface.
        SendMessageAPI service = retrofit.create(SendMessageAPI.class);
        String uniqueID = UUID.randomUUID().toString();
        Call<SentMessage> call = service.sendMessage(cryted_Action, toUser.userID, AppView.instance.userdata.UserID, krypted_msg, uniqueID);

        call.enqueue(new Callback<SentMessage>() {

            @Override
            public void onResponse(Response<SentMessage> response) {
                Log.e("Response code ", String.valueOf(response.code()));
                SentMessage result = response.body();
                if (result == null) {
                    Log.e("MSG-SENT", "invalid result body from server");
                    return;
                }

                String value = result.result;
                if (value.equals("true")) {
                    //MSG SENT WITH SUCCESS
                    textMsg.setText(null);
                    btn_sendMsg.setEnabled(true);

                    ChatMessage msg_obj = new ChatMessage(false, toUser.userID, msg, result.uID);
                    AddMessage(msg_obj);
                    //TODO chatArrayAdapter.add(msg_obj);
                    String resend = getResources().getString(R.string.btn_send_send);
                    btn_sendMsg.setText(resend);
                    btn_sendMsg.setEnabled(true);
                }
                if (value.equals("false")) {
                    //FAILED TO SEND MESSAGE
                    String resend = getResources().getString(R.string.btn_send_retry);
                    btn_sendMsg.setText(resend);
                    btn_sendMsg.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("Response3 code ", String.valueOf(t));
                MainActivity.mainactivity.no_service("FAILED-MSG", "FAILED TO SEND MESSAGE!!!");
                System.out.println(t.getMessage());
                System.out.println(t.getCause());
                //FAILED MESSAGE RETRY
                String resend = getResources().getString(R.string.btn_send_retry);
                btn_sendMsg.setText(resend);
                btn_sendMsg.setEnabled(true);
            }
        });
    }

    public void AddMessage(ChatMessage msg) {
        //addStoredMessage
        StorePrefs storage = new StorePrefs();
        storage.addStoredMessage(AppView.instance, msg.fromUser, toUser.username, toUser.description, 0, msg);
        UpdateArrayAdapter();
        /*
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //stuff that updates ui
                String msg = extras.getString("message");
                Integer fromID = Integer.valueOf(extras.getString("from_ID"));
                String uniqueID = extras.getString("uniqueID");
                ChatMessage msg_obj = new ChatMessage(true, fromID, msg, uniqueID);
                instance.chatArrayAdapter.add(msg_obj);
            }
        });
        */
    }

    public void UpdateArrayAdapter() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                StorePrefs storage = new StorePrefs();
                //Log.e("UPDATE-CHAT", "UPDATING CHAT WINDOW FOR ID: " + toUser.userID);
                StoreMessage stored_messages = storage.getMessagesFromID(getActivity(), toUser.userID);
                //stuff that updates ui
                if (stored_messages != null) {
                    //Log.e("UPDATE-CHAT", "messages found: " + stored_messages.messages.size());
                    chatArrayAdapter.setChatMessageList(stored_messages.messages);
                    if(stored_messages.messages.size() > 0) {
                        toggleUserDetails(false);
                    } else {
                        //no messages stored
                        toggleUserDetails(true);
                    }
                } else {
                    //Log.e("UPDATE-CHAT", "NO MESSAGES FOUND FOR USER" + toUser.userID);
                    toggleUserDetails(true);
                }
            }
        });
    }

    public void toggleUserDetails(Boolean isVisible) {
        if(isVisible) {
            userName.setVisibility(View.VISIBLE);
            aboutU.setVisibility(View.VISIBLE);
        } else {
            userName.setVisibility(View.GONE);
            aboutU.setVisibility(View.GONE);
        }
    }

}
