package com.srk.apps.omni.Fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.srk.apps.omni.Activitys.AppView;
import com.srk.apps.omni.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ScanResults.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ScanResults#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ScanResults extends Fragment implements  View.OnClickListener{

    public static ScanResults instance;
    public int[] usersFound;
    public String rawList;

    public ScanResults() {
        // Required empty public constructor
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ScanedResults.
     */
    // TODO: Rename and change types and number of parameters
    public static ScanResults newInstance(String param1, String param2) {
        ScanResults fragment = new ScanResults();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
        Log.d("SCAN", "scan fragment created");
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void onClick(final View v) { //check for what button is pressed
        switch (v.getId()) {
            case R.id.usersResults:

                //usersFound
                Log.e("CLICK", "USERS FOUND: " + usersFound.length + " rawList: " + rawList );
                //TODO OPEN USERS SCANED FRAGMENT
                 //send rawList results to fragment
                AppView.instance.OpenUserResultsFragment(rawList);
                break;
        }
    }

    View myInflatedView;

    public FrameLayout usersResults;
    public TextView countedUsers;

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        myInflatedView = inflater.inflate(R.layout.fragment_scan_results, container, false);

        Log.i("VIEW", "onCreateView fragment finished");
        // Inflate the layout for this fragment
        Bundle args = getArguments();
        if (args != null) {
            Integer usersCounter = args.getInt("usersCount", 0);
            Log.i("VIEW", "onCreateView count: " + usersCounter);

            if( usersCounter > 0) {
                usersResults = (FrameLayout) myInflatedView.findViewById(R.id.usersResults);
                usersResults.setVisibility(View.VISIBLE);
                usersResults.setOnClickListener(this);
                countedUsers = (TextView) usersResults.findViewById(R.id.countedUsers);

                countedUsers.setText(usersCounter.toString());
                usersFound = args.getIntArray("usersFound");
                rawList = args.getString("rawList");
            }
            //PARSE OTHER RESULTS
        }
        return myInflatedView;
    }
/*
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    */
/*
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
*/

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
