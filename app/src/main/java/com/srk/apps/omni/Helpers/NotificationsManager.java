package com.srk.apps.omni.Helpers;

import android.content.Context;

import com.srk.apps.omni.StorageClass.NotificationItem;
import com.srk.apps.omni.StorageClass.StorePrefs;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * OMNI
 * by SRK on 12-02-2016 @ 17:11
 *
 * @SYNAPPS 2016
 */
public class NotificationsManager implements Serializable{

    public List<NotificationItem> NotificationsList = new ArrayList<>();

    public NotificationsManager() {
        NotificationsList = new ArrayList<>();
    }

    public void addItem(NotificationItem item) {
        if(NotificationsList == null) {
            NotificationsList = new ArrayList<>();
        }
        if(NotificationsList.contains(item)) {
            //item already added to list, do something ?
        } else  {
            NotificationsList.add(item);
        }
    }

    public void removeItem(Context ctx, String uniqueID) {
        StorePrefs storage = new StorePrefs();
        storage.RemoveNotification(ctx, uniqueID);
    }

}
